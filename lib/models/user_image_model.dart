class UserImage {
  int id;
  int idImage;
  int idUser;
  String path;
  int principal;

  UserImage({
    this.id,
    this.idImage,
    this.idUser,
    this.path,
    this.principal,
  });

  factory UserImage.fromJson(Map<String, dynamic> json) => UserImage(
        id: json["id"],
        idImage: json["idImage"],
        idUser: json["idUser"],
        path: json["path"],
        principal: json["principal"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "idImage": idImage,
        "idUser": idUser,
        "path": path,
        "principal": principal,
      };
}
