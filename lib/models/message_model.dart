class Message {
  int id;
  int idChat;
  int idSender;
  int idMessage;
  String message;
  String createdAt;

  Message({
    this.id,
    this.idChat,
    this.idMessage,
    this.idSender,
    this.message,
    this.createdAt,
  });


  factory Message.fromJson(Map<String, dynamic> json) => Message(
        id: json["id"],
        idChat: json["idChat"],
        idMessage: json["idMessage"],
        idSender: json["idSender"],
        message: json["message"],
        createdAt: json["createdAt"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "idChat": idChat,
        "idMessage": idMessage,
        "idSender": idSender,
        "message": message,
        "createdAt": createdAt,
      };


}

