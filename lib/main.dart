import 'package:admob_flutter/admob_flutter.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vegwe/SharedFunctions.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/pages/introduction.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vegwe/providers/FirebaseMessagingService.dart';

// -+-+-++-++-+--++-+-+-+--+-++-+--+-+-+-+-+-+-+-++-+-+-
import 'package:vegwe/routes/routes.dart';
import 'package:easy_localization/easy_localization.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Initialize without device test ids

  runApp(EasyLocalization(
    child: MyApp(),
    supportedLocales: [
      Locale('en', 'US'),
      Locale('es', 'ES'),
      Locale('pt', 'BR')
    ], // [Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hant', countryCode: 'HK')]
    path: 'assets/i18n',
    fallbackLocale: Locale('en', 'US'),

    // saveLocale: false,
    // useOnlyLangCode: true,
    // preloaderColor: Colors.black,
  ));

  // FirebaseAdMob.instance
  //     .initialize(appId: 'ca-app-pub-1778889728876239~9610005056');
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    print("myBackgroundMessageHandler");
    print(message);
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    print("myBackgroundMessageHandler");
    print(message);
  }

  // Or do other work.
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

    FirebaseMessagingService.messaging.firebaseMessaging.configure(
      onMessage: (data) async {
        print("onMessage");
        print(data);
      },
      onBackgroundMessage: myBackgroundMessageHandler,
      onResume: (data) async {
        print("onResume");
        print(data);
      },
      onLaunch: (data) async {
        print("onLaunch");
        print(data);
      },
    );
    FirebaseMessagingService.messaging.firebaseMessaging
        .requestNotificationPermissions();
    FirebaseMessagingService.messaging.firebaseMessaging
        .getToken()
        .then((value) => print('Token firemess $value'));
  }

  @override
  Widget build(BuildContext context) {
    // var localizationDelegate = LocalizedApp.of(context).delegate;
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp
    ]); //Restringe la orientacion de la pantalla a solo vertical
    return Provider(
      child: GetMaterialApp(
        locale: EasyLocalization.of(context).locale,
        localizationsDelegates: EasyLocalization.of(context).delegates,
        supportedLocales: EasyLocalization.of(context).supportedLocales,
        theme: ThemeData(
          textTheme:
              GoogleFonts.andikaTextTheme(Theme.of(context).textTheme),
        ),
        title: 'LetsVegan',
        debugShowCheckedModeBanner: false,
        // initialRoute: 'introduction',
        home: SplashScreen.navigate(
          name: 'assets/vegwe.flr',
          next: (_) => IntroduccionPage(),
          until: () async {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            if (prefs.getString('api_token') != null &&
                prefs.getString('api_token') != '') {
              // Navigator.pushNamed(context, 'social');
              Get.toNamed("social");
            }
          },
          startAnimation: 'first',
          loopAnimation: 'loop',
        ),
        // routes: getAplicationRoutes(),
        onGenerateRoute: (RouteSettings routeSettings) =>
            getRoutesBuilderFunction(routeSettings),
      ),
    );
  }
}
