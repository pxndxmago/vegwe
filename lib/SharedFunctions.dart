import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/pages/fragments/social/Cards.dart';
import 'package:vegwe/providers/api_provider.dart';

class SharedFunctions {
  static final SharedFunctions f = SharedFunctions._();
  SharedFunctions._();
  BuildContext _context;
  set context(BuildContext context) => {_context = context};

  int calculateAge(String bornDate) {
    DateTime currentDate = DateTime.now();
    DateTime birthDate = DateFormat('yyyy-M-dd').parse(bornDate);

    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  void showSnackbar(String title, String text) {
    Get.snackbar(title, text,
        snackPosition: SnackPosition.BOTTOM,
        margin: EdgeInsets.all(0),
        snackStyle: SnackStyle.GROUNDED,
        borderRadius: 0,
        overlayBlur: 0,
        barBlur: 80,
        backgroundColor: Colors.black45,
        colorText: Colors.white70);
  }

  Future<void> loadCards() async {
    print("Se piden 5 perfiles");
    Map response = await ApiProvider.api.getUsers();
    // print(response);
    List<Profile> temp = [];
    if (response['status'] == 'success') {
      for (var profile in response['users']) {
        int age = calculateAge(profile['bornDate']);
        if ((profile['image_users'] as List).isEmpty) continue;

        Profile newProfile = new Profile(
          idUser: profile['idUser'],
          names: profile['names'],
          surnames: profile['surnames'],
          city: profile['city'].toString(),
          country: profile['country'].toString(),
          age: age.toString(),
          description: profile['description'],
          gender: profile['gender'],
          interestedIn: profile['interestedIn'],
          interests: profile['interests'],
          job: profile['job'],
          lifeStyle: profile['lifeStyle'],
          location: profile['location'],
          lookingFor: profile['lookingFor'],
          userImages: profile['image_users'],
        );
        temp.add(newProfile);
      }
      Provider.of(_context).changeProfiles(temp);
      print('loadCards');
    } else {}
  }
}
