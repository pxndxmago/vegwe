import 'dart:io';

class AdMobService {
  static final AdMobService admob = AdMobService._();
  AdMobService._();
  bool test = true;
  
  String getAdMobAppId() {
    if (Platform.isAndroid) {
      return 'ca-app-pub-1778889728876239~9610005056';
    } else if (Platform.isIOS) {
      return 'ca-app-pub-1778889728876239~9610005056';
    }
  }

  String getBannerId() {
    if (test) return 'ca-app-pub-3940256099942544/6300978111';
    if (Platform.isAndroid) {
      return 'ca-app-pub-1778889728876239/6387061446';
    } else if (Platform.isIOS) {
      return 'ca-app-pub-1778889728876239/6387061446';
    }
  }
  String getIntersticialId() {
    if (test) return 'ca-app-pub-3940256099942544/1033173712';
    if (Platform.isAndroid) {
      return 'ca-app-pub-1778889728876239/8039054465';
    } else if (Platform.isIOS) {
      return 'ca-app-pub-1778889728876239/8039054465';
    }
  }
}
