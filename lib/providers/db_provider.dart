import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:vegwe/models/message_model.dart';
import 'package:vegwe/models/user_image_model.dart';
export 'package:vegwe/models/message_model.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();
  DBProvider._();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  destroyDb() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, "LetsVegan_m.db");
    File db = new File(path);
    if (await db.exists()) {
      db.delete();
    }
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    final path = join(documentsDirectory.path, "LetsVegan_m.db");
    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        await db.execute("CREATE TABLE messages ("
            " id INTEGER PRIMARY KEY,"
            " idMessage INTEGER,"
            " idChat INTEGER,"
            " idSender INTEGER,"
            " message TEXT,"
            " createdAt TEXT"
            ")");
        await db.execute("CREATE TABLE user_images ("
            " id INTEGER PRIMARY KEY,"
            " idImage INTEGER,"
            " idUser INTEGER,"
            " path TEXT,"
            " principal INTEGER"
            ")");
      },
    );
  }

  Future<int> insertMessage(Message message) async {
    final db = await database;
    var res = 0;
    try {
      var resM = await db.query('messages',
          where: 'idMessage=?', whereArgs: [message.idMessage]);
      if (resM.isEmpty) {
        res = await db.insert('messages', message.toJson());
      } else {
        // print("Ya existe un registro con este idMessage $resM");
      }
    } catch (e) {
      print(e);
    }
    return res;
  }

  Future<Message> getMessage(int id) async {
    final db = await database;
    final res = await db.query('messages', where: 'id=?', whereArgs: [id]);

    return res.isNotEmpty ? Message.fromJson(res.first) : null;
  }

  Future<List<Message>> getAllMessages() async {
    final db = await database;
    final res = await db.query('messages');
    List<Message> messages = res.isNotEmpty
        ? res.map((message) => Message.fromJson(message)).toList()
        : [];
    return messages;
  }

  Future<List<Message>> getMessagesByChat(
      int idChat, int idMessage, int limit) async {
    var whereArgs = [];
    whereArgs.add(idChat);
    if (idMessage != null && idMessage != 0) whereArgs.add(idMessage);

    final db = await database;
    final res = await db.query('messages',
        where: 'idChat=? ' +
            (idMessage != null && idMessage != 0 ? 'AND idMessage < ?' : ''),
        whereArgs: whereArgs,
        orderBy: "idMessage desc",
        limit: limit > 0 ? limit : 10);
    List<Message> messages = res.isNotEmpty
        ? res.map((message) => Message.fromJson(message)).toList()
        : [];
    return messages;
  }

  Future<int> insertUserImage(UserImage userImage) async {
    final db = await database;
    var res = 0;
    try {
      var resM = await db.query('user_images',
          where: 'idImage=?', whereArgs: [userImage.idImage]);
      if (resM.isEmpty) {
        res = await db.insert('user_images', userImage.toJson());
      } else {
        print("Ya existe un registro con este idImage $resM");
      }
    } catch (e) {
      print(e);
    }
    return res;
  }

  Future<UserImage> getUserImage(int idImage) async {
    final db = await database;
    final res =
        await db.query('user_images', where: 'idImage=?', whereArgs: [idImage]);

    return res.isNotEmpty ? UserImage.fromJson(res.first) : null;
  }
}
