import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'package:vegwe/models/message_model.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/providers/api_provider.dart';
import 'package:vegwe/providers/db_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vegwe/SharedFunctions.dart';
export 'package:vegwe/models/message_model.dart';

class SocketIOProvider {
  static SocketIO _socketIO;
  static final SocketIOProvider io = SocketIOProvider._();
  SocketIOProvider._();
  BuildContext _context;

  set context(BuildContext context) => {_context = context};

  Future<SocketIO> socketIO() async {
    if (_socketIO != null) return _socketIO;
    // await initSocketIO();
    print('Se realiza conexion con socketIO desde el SocketIOProvider');
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    int idUser = pref.getInt('idUser');
    print("Token extraido en initSocketIO $token");
    _socketIO = SocketIOManager().createSocketIO(
      ApiProvider.socketIOUrl,
      "/room",
      query: "token=$token&idUser=$idUser",
      // query: "userId=10",
      socketStatusCallback: _socketStatus,
    );
    _socketIO.init();
    _socketIO.subscribe("new_message", _getMessage);
    _socketIO.subscribe("socket_connections", _getConnections);

    //connect socket
    _socketIO.connect();
    return _socketIO;
  }

  // initSocketIO() async {
  //   print('Se realiza conexion con socketIO desde el SocketIOProvider');
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   String token = pref.getString('api_token');
  //   int idUser = pref.getInt('idUser');
  //   print("Token extraido en initSocketIO $token");
  //   _socketIO = SocketIOManager().createSocketIO(
  //     ApiProvider.socketIOUrl,
  //     "/room",
  //     query: "token=$token&idUser=$idUser",
  //     // query: "userId=10",
  //     socketStatusCallback: _socketStatus,
  //   );
  //   _socketIO.init();
  //   _socketIO.subscribe("new_message", _getMessage);
  //   _socketIO.subscribe("socket_connections", _getConnections);

  //   //connect socket
  //   _socketIO.connect();
  // }

  endSOcketIO() async {
    await SocketIOManager().destroySocket(_socketIO);
    // await _socketIO?.disconnect();
    // await _socketIO?.unSubscribesAll();
    _socketIO = null;
    _context = null;
  }

  _getMessage(dynamic data) async {
    Map<String, dynamic> map = new Map<String, dynamic>();
    map = json.decode(data);
    final newMessage = Message(
      idChat: map['idChat'],
      idMessage: map['idMessage'],
      idSender: map['idSender'],
      message: map['message'],
      createdAt: map['createdAt'],
    );

    int respSave = await DBProvider.db.insertMessage(newMessage);
    if (respSave > 0) {
      print("mensaje guardado en sqlite");
      if (_context != null) {
        Message mess = await DBProvider.db.getMessage(respSave);
        List messages = Provider.of(_context).messages;
        messages.add(mess);
        Provider.of(_context).changeMessages(messages);
      }
    } else {
      print('no se guardo en sqlite');
    }
    print('Mensaje recibido: ${newMessage.message}');
  }

  void _getConnections(dynamic data) {
    // print(data);
    // Map<String, dynamic> map = new Map<String, dynamic>();
    // map = json.decode(data);
    // setState(() => conn = map['socket']);
  }

  _socketStatus(dynamic data) {
    // print("Socket status: " + data);
  }
}
