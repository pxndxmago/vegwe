import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vegwe/models/user_image_model.dart';
import 'package:vegwe/providers/db_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decode/jwt_decode.dart';
import 'package:http_parser/http_parser.dart';

class ApiProvider {
  static String protocol = "http";
  static String host = "192.168.100.34"; //wifi
  // static String host = "192.168.100.38"; //ethernet
  // static String host = "192.168.100.42"; // servercloned
  // static String host = "3.23.131.0"; // aws

  static String hostname = "$protocol://$host";
  static String apiPort = "3002";
  static String socketIOPort = "3003";
  static String apiURL = "$hostname:$apiPort";
  static String socketIOURL = "$hostname:$socketIOPort";

  static String signUpUrl = "$apiURL/api/signUp";
  static String checkMailUrl = "$apiURL/api/checkMail";
  static String loginUrl = "$apiURL/api/login";
  static String loginWithFacebookUrl = "$apiURL/api/loginWithFacebook";
  static String loginWithGoogleUrl = "$apiURL/api/loginWithGoogle";
  static String chatsUrl = "$apiURL/api/chats";
  static String socketIOUrl = "$socketIOURL";
  static String messagesUrl = "$apiURL/api/messages";
  static String usersUrl = "$apiURL/api/users";
  static String likesUrl = "$apiURL/api/likes";
  static String interestsUrl = "$apiURL/api/interests";
  static String countriesUrl = "$apiURL/api/countries";
  static String citiesUrl = "$apiURL/api/cities";
  static String userImagesUrl = "$apiURL/api/userImages";
  static String uploadImageUrl = "$apiURL/api/uploadImage";

  static final ApiProvider api = ApiProvider._();
  ApiProvider._();

  Future<Map> getUser([int idUserShow = 0]) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    int idUser = idUserShow == 0 ? pref.getInt('idUser') : idUserShow;
    String token = pref.getString('api_token');
    try {
      String url = "$usersUrl/${idUser.toString()}";
      var response = await http.get(url,
          headers: {'x-access-token': token}).timeout(Duration(seconds: 5));
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in getUser method: $err");
    }
    return responseMap;
  }

  // Future<Map> getUsers() async {
  //   Map responseMap;
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   String token = pref.getString('api_token');
  //   try {
  //     String url = "$usersUrl";
  //     var response = await http.get(url,
  //         headers: {'x-access-token': token}).timeout(Duration(seconds: 2));
  //     responseMap = _response(response);
  //   } on TimeoutException {
  //     throw FetchDataException('Timeout exceeded');
  //   } on SocketException {
  //     throw FetchDataException('No Internet connection');
  //   } catch (err) {
  //     print("Error in getUsers method: $err");
  //   }
  //   return responseMap;
  // }
  Future<Map> getUsers({limit = 20}) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    try {
      String url = "$usersUrl";
      var queryParams = {
        'gender': '',
        'lifestyle': '',
      };
      List gender = [];
      List lifestyle = [];
      if (pref.getBool('gender.male') != null && pref.getBool('gender.male')) {
        gender.add('male');
      }

      if (pref.getBool('gender.female') != null &&
          pref.getBool('gender.female')) {
        gender.add('female');
      }

      if (pref.getBool('gender.other') != null &&
          pref.getBool('gender.other')) {
        gender.add('other');
      }

      queryParams['gender'] = gender.join(',');

      if (pref.getBool('lifestyle.vegan') != null &&
          pref.getBool('lifestyle.vegan')) {
        lifestyle.add('vegan');
      }
      if (pref.getBool('lifestyle.vegetarian') != null &&
          pref.getBool('lifestyle.vegetarian')) {
        lifestyle.add('vegetarian');
      }
      if (pref.getBool('lifestyle.interested') != null &&
          pref.getBool('lifestyle.interested')) {
        lifestyle.add('interested');
      }
      queryParams['lifestyle'] = lifestyle.join(',');
      queryParams['limit'] = limit.toString();
      // print('QueryParams');
      // print(json.encode(queryParams));
      var uri;
      if (protocol == 'http')
        uri = Uri.http('$host:$apiPort', '/api/users', queryParams);
      else
        uri = Uri.https('$host:$apiPort', '/api/users', queryParams);

      // printInfo(uri.toString());
      var response = await http.get(uri,
          headers: {'x-access-token': token}).timeout(Duration(seconds: 2));
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in getUsers method: $err");
    }
    return responseMap;
  }

  Future<Map> updateUser({
    names = '',
    surnames = '',
    bornDate = '',
    country = '',
    city = '',
    location = '',
    description = '',
    interests = '',
    languages = '',
    lifeStyle = '',
    lookingFor = '',
    interestedIn = '',
    gender = '',
    job = '',
  }) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    try {
      String url = "$usersUrl";
      Map body = {};
      if (names != null && names != '') body['names'] = names;
      if (surnames != null && surnames != '') body['surnames'] = surnames;
      if (bornDate != null && bornDate != '') body['bornDate'] = bornDate;
      if (country != null && country != '') body['country'] = country;
      if (city != null && city != '') body['city'] = city;
      if (location != null && location != '') body['location'] = location;
      if (description != null && description != '')
        body['description'] = description;
      if (interests != null && interests != '') body['interests'] = interests;
      if (languages != null && languages != '') body['languages'] = languages;
      if (lifeStyle != null && lifeStyle != '') body['lifeStyle'] = lifeStyle;
      if (lookingFor != null && lookingFor != '')
        body['lookingFor'] = lookingFor;
      if (interestedIn != null && interestedIn != '')
        body['interestedIn'] = interestedIn;
      if (gender != null && gender != '') body['gender'] = gender;
      if (job != null && job != '') body['job'] = job;

      var response = await http.put(url,
          body: body,
          headers: {'x-access-token': token}).timeout(Duration(seconds: 2));
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in getUser method: $err");
    }
    return responseMap;
  }

  Future<Map> login(String email, String password,
      {double latitude, double longitude}) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      Map body = {
        'email': email,
        'password': password,
      };
      if (latitude != null) body['latitude'] = latitude.toString();
      if (longitude != null) body['longitude'] = longitude.toString();

      var response = await http.post(loginUrl, body: body);
      responseMap = _response(response);
      if (responseMap['status'] == 'success') {
        await pref.setString('api_token', responseMap['api_token']);
        print('Login Response');
        print(' $responseMap');
        var payload = Jwt.parseJwt(responseMap['api_token']);
        // print("PAYLOAD");
        // print(payload);
        await pref.setInt('idUser', payload['idUser']);
      }
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in login method: $err");
    }
    return responseMap;
  }

  Future<Map> loginWithFacebook(String token,
      {double latitude, double longitude}) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      Map body = {};
      if (latitude != null) body['latitude'] = latitude.toString();
      if (longitude != null) body['longitude'] = longitude.toString();

      var response = await http.post(loginWithFacebookUrl,
          headers: {'fb-token': token}, body: body);
      responseMap = _response(response);
      if (responseMap['status'] == 'success') {
        await pref.setString('api_token', responseMap['api_token']);
        print('Login Response');
        print(' $responseMap');
        var payload = Jwt.parseJwt(responseMap['api_token']);
        print("PAYLOAD");
        print(payload);
        await pref.setInt('idUser', payload['idUser']);
      }
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in login method: $err");
    }
    return responseMap;
  }

  Future<Map> loginWithGoogle(String token,
      {double latitude, double longitude}) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      Map body = {};
      if (latitude != null) body['latitude'] = latitude.toString();
      if (longitude != null) body['longitude'] = longitude.toString();

      var response = await http.post(loginWithGoogleUrl,
          headers: {'google-token': token}, body: body);
      responseMap = _response(response);
      if (responseMap['status'] == 'success') {
        await pref.setString('api_token', responseMap['api_token']);
        print('Login Response');
        print(' $responseMap');
        var payload = Jwt.parseJwt(responseMap['api_token']);
        await pref.setInt('idUser', payload['idUser']);
      }
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in login method: $err");
    }
    return responseMap;
  }

  Future<Map> signup(
      {String names,
      String surnames,
      String bornDate,
      String gender,
      String interestedIn,
      String lookingFor,
      String lifeStyle,
      String password,
      String email}) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    try {
      var response = await http.post(signUpUrl, body: {
        "names": names,
        "surnames": surnames,
        "bornDate": bornDate,
        "gender": gender,
        "interestedIn": interestedIn,
        "lookingFor": lookingFor,
        "lifeStyle": lifeStyle,
        "password": password,
        "email": email
      });
      responseMap = _response(response);
      if (responseMap['status'] == 'success') {
        await pref.setString('api_token', responseMap['api_token']);
        var payload = Jwt.parseJwt(responseMap['api_token']);
        await pref.setInt('idUser', payload['idUser']);
      }
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in signup method: $err");
    }
    return responseMap;
  }

  Future<Map> checkMail(String email) async {
    Map responseMap;
    try {
      var response = await http.post(checkMailUrl, body: {"email": email});
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in checkMail method: $err");
    }
    return responseMap;
  }

  Future<Map> getChats() async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    try {
      String url = "$chatsUrl";
      var response = await http.get(url, headers: {'x-access-token': token});
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in checkMail method: $err");
    }
    return responseMap;
  }

  Future<Map> getChat(int idChat) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    try {
      String url = "$chatsUrl/$idChat";
      var response = await http.get(url, headers: {'x-access-token': token});
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in checkMail method: $err");
    }
    return responseMap;
  }

  Future<Map> createChat(int idUserChat) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    try {
      String url = "$chatsUrl";
      var response = await http.post(url, headers: {
        'x-access-token': token
      }, body: {
        'nameChat': '',
        'userChat': idUserChat.toString(),
      });
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in createChat method: $err");
    }
    return responseMap;
  }

  Future<Map> getMessages(int idChat, [dynamic idMessage = '']) async {
    Map responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    try {
      String url = "$messagesUrl/?idChat=$idChat&idMessage=$idMessage&limit=10";
      var response = await http.get(url,
          headers: {'x-access-token': token}).timeout(Duration(seconds: 2));
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in getMessages method: $err");
    }
    return responseMap;
  }

  Future<File> getUserImage(int idImage, String filename) async {
    http.Client client = new http.Client();

    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    int idUser = pref.getInt('idUser');
    File file;
    try {
      UserImage userImage = await DBProvider.db.getUserImage(idImage);

      if (userImage != null) {
        print("Imagen encontrada no es necesario descargarla");
        file = File(userImage.path);
      } else {
        String url = "$userImagesUrl/$idImage";
        var response = await client.get(url,
            headers: {'x-access-token': token}).timeout(Duration(seconds: 15));

        var bytes = response.bodyBytes;

        Map<String, String> headers = response.headers;
        // print(headers);
        String finalFilename =
            (headers['content-type'] == 'image/jpeg') ? 'jpg' : '';
        String dir = (await getApplicationDocumentsDirectory()).path;
        file = new File('$dir/$filename.$finalFilename');
        await file.writeAsBytes(bytes);

        UserImage newUserImage = UserImage(
            idImage: idImage, idUser: idUser, principal: 0, path: file.path);
        int resp = await DBProvider.db.insertUserImage(newUserImage);
        if (resp > 0) print('Se ingresa en sqlite el archivo descargado');
      }
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in getMessages method: $err");
    }
    return file;
  }

  Future<http.StreamedResponse> uploadUserImage(
      File file, bool principal) async {
    http.StreamedResponse responseMap;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    int idUser = pref.getInt('idUser');

    try {
      String url = "$uploadImageUrl";
      var request = http.MultipartRequest('POST', Uri.parse(url));
      request.fields['type'] = 'user';
      request.fields['idOfType'] = idUser.toString();
      request.fields['principal'] = principal ? '1' : '0';
      request.headers['x-access-token'] = token;

      request.files.add(await http.MultipartFile.fromPath(
          'imageSend', file.path,
          contentType: MediaType('image', 'jpeg')));
      var response = await request.send();
      responseMap = response;
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in checkMail method: $err");
    }
    return responseMap;
  }

  Future<Map> sendLike(int idUserLiked, int fLiked) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    Map responseMap;
    try {
      var response = await http.post(likesUrl, body: {
        'idUserLiked': idUserLiked.toString(),
        'f_liked': fLiked.toString()
      }, headers: {
        "x-access-token": token
      });
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in sendLike method: $err");
    }
    return responseMap;
  }

  Future<Map> deleteUserImage(int idImage) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    Map responseMap;
    String url = "$userImagesUrl/$idImage";
    try {
      var response = await http.delete(url, headers: {"x-access-token": token});
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in sendLike method: $err");
    }
    return responseMap;
  }

  Future<Map> getLikes() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    Map responseMap;
    try {
      var response =
          await http.get(likesUrl, headers: {"x-access-token": token});
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in sendLike method: $err");
    }
    return responseMap;
  }

  Future<Map> getInterests(BuildContext context) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString('api_token');
    Map responseMap;
    String locale = context.locale.toString();
    String url = "$interestsUrl/$locale";
    try {
      var response = await http.get(url, headers: {"x-access-token": token});
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in getInterests method: $err");
    }
    return responseMap;
  }

  Future<Map> getCountries() async {
    // SharedPreferences pref = await SharedPreferences.getInstance();
    // String token = pref.getString('api_token');
    Map responseMap;
    String url = "$countriesUrl";
    try {
      var response = await http.get(
        url,
        // headers: {"x-access-token": token},
      );
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in getInterests method: $err");
    }
    return responseMap;
  }

  Future<Map> getCities(int countryId) async {
    // SharedPreferences pref = await SharedPreferences.getInstance();
    // String token = pref.getString('api_token');
    Map responseMap;
    String url = "$citiesUrl/$countryId";
    try {
      var response = await http.get(
        url,
        // headers: {"x-access-token": token},
      );
      responseMap = _response(response);
    } on TimeoutException {
      throw FetchDataException('Timeout exceeded');
    } on SocketException {
      throw FetchDataException('No Internet connection');
    } catch (err) {
      print("Error in getInterests method: $err");
    }
    return responseMap;
  }

  Map _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        Map responseJson = json.decode(response.body.toString());
        // print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}

class CustomException implements Exception {
  final _message;
  final _prefix;

  CustomException([this._message, this._prefix]);

  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends CustomException {
  FetchDataException([String message])
      : super(message, "Error During Communication: ");
}

class BadRequestException extends CustomException {
  BadRequestException([message]) : super(message, "Invalid Request: ");
}

class UnauthorisedException extends CustomException {
  UnauthorisedException([message]) : super(message, "Unauthorised: ");
}

class InvalidInputException extends CustomException {
  InvalidInputException([String message]) : super(message, "Invalid Input: ");
}
