import 'package:flutter/material.dart';
import 'package:liquid_swipe/liquid_swipe.dart';

class IntroduccionPage extends StatefulWidget {
  IntroduccionPage({Key key}) : super(key: key);

  @override
  _IntroduccionPageState createState() => _IntroduccionPageState();
}

class _IntroduccionPageState extends State<IntroduccionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LiquidSwipe(
        enableLoop: false,
        initialPage: 0,
        enableSlideIcon: true,
        pages: <Container>[
          firstPage(),
          secondPage(),
          thirdPage(),
          fourthPage()
        ],
      ),
    );
  }

  Widget firstPage() {
    return Container(
      color: Colors.black,
      child: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: <Widget>[
            RaisedButton(
              color: Colors.transparent,
              elevation: 0,
              onPressed: () {
                Navigator.pushNamed(context, "login");
              },
              child: Text("Skip"),
            )
          ],
        ),
        // body: FlareActor('assets/vegwe.flr',
        //     alignment: Alignment.center,
        //     fit: BoxFit.contain,
        //     animation: "first"),
      ),
    );
  }

  Widget secondPage() {
    return Container(
      color: Colors.red,
      child: Scaffold(
        backgroundColor: Colors.purple,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: <Widget>[
            RaisedButton(
              color: Colors.transparent,
              elevation: 0,
              onPressed: () {
                Navigator.pushNamed(context, "loginSignUp");
              },
              child: Text("Skip"),
            )
          ],
        ),
      ),
    );
  }

  Widget thirdPage() {
    return Container(
      color: Colors.purple,
      child: Scaffold(
        backgroundColor: Colors.deepPurple,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: <Widget>[
            RaisedButton(
              color: Colors.transparent,
              elevation: 0,
              onPressed: () {
                Navigator.pushNamed(context, "loginSignUp");
              },
              child: Text("Skip"),
            )
          ],
        ),
      ),
    );
  }

  Widget fourthPage() {
    return Container(
      color: Colors.white,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: <Widget>[
            RaisedButton(
              color: Colors.transparent,
              elevation: 0,
              onPressed: () {
                Navigator.pushNamed(context, "loginSignUp");
              },
              child: Text("Done"),
            )
          ],
        ),
      ),
    );
  }
}
