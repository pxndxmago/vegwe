import 'dart:async';
import 'package:vegwe/models/message_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:vegwe/pages/fragments/social/Cards.dart';

class SocialPageBloc {
  var _bodyFragment = BehaviorSubject<String>.seeded("cards");
  var _selectedChat = BehaviorSubject<int>.seeded(0);
  var _chats = BehaviorSubject<List>.seeded([]);
  var _messages = BehaviorSubject<List<Message>>.seeded([]);
  var _profiles = BehaviorSubject<List<Profile>>.seeded([]);

  // Recuperar datos del stream
  Stream<String> get bodyFragmentStream => _bodyFragment.stream;
  Stream<int> get selectedChatStream => _selectedChat.stream;
  Stream<List> get chatsStream => _chats.stream;
  Stream<List<Message>> get messagesStream => _messages.stream;
  Stream<List<Profile>> get profilesStream => _profiles.stream;

  // Insertar datos en el stream
  Function(String) get changeBodyFragment => _bodyFragment.sink.add;
  Function(int) get changeSelectedChat => _selectedChat.sink.add;
  Function(List) get changeChats => _chats.sink.add;
  Function(List<Message>) get changeMessages => _messages.sink.add;
  Function(List<Profile>) get changeProfiles => _profiles.sink.add;

  // Obtener el ultimo valor de un stream
  String get bodyFragment => _bodyFragment.value;
  int get selectedChat => _selectedChat.value;
  List get chats => _chats.value;
  List<Message> get messages => _messages.value;
  List<Profile> get profiles => _profiles.value;

  dispose() {
    _bodyFragment?.close();
    _selectedChat?.close();
    _chats?.close();
    _messages?.close();
    _profiles?.close();
  }

  clear() {
    _bodyFragment = BehaviorSubject<String>.seeded("cards");
    _selectedChat = BehaviorSubject<int>.seeded(0);
    _chats = BehaviorSubject<List>.seeded([]);
    _messages = BehaviorSubject<List<Message>>.seeded([]);
    _profiles = BehaviorSubject<List<Profile>>.seeded([]);
  }
}
