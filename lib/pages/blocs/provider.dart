import 'package:flutter/material.dart';
import 'package:vegwe/pages/blocs/socialpage_bloc.dart';
export 'package:vegwe/pages/blocs/socialpage_bloc.dart';

class Provider extends InheritedWidget {
  final socialPageBloc = SocialPageBloc();

  Provider({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static SocialPageBloc of(BuildContext context) {
    return (context.dependOnInheritedWidgetOfExactType<Provider>())
        .socialPageBloc;
  }
}
