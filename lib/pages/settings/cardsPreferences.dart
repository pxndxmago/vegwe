import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/SharedFunctions.dart';
import 'package:vegwe/pages/fragments/social/Cards.dart';
import 'package:vegwe/providers/api_provider.dart';

class CardsPreferencesPage extends StatefulWidget {
  CardsPreferencesPage({Key key}) : super(key: key);

  @override
  _CardsPreferencesPageState createState() => _CardsPreferencesPageState();
}

class _CardsPreferencesPageState extends State<CardsPreferencesPage> {
  SharedPreferences pref;
  SocialPageBloc bloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    pref = await SharedPreferences.getInstance();
    print('didChangeDependencies de cardsPreferences');
    bloc ??= Provider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      body: Builder(
        builder: (context) {
          return GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  // expandedHeight: MediaQuery.of(context).size.height * 0.5,
                  title: Text(
                    tr("settings.cardsPreferences"),
                    style: Theme.of(context)
                        .textTheme
                        .headline6
                        .copyWith(color: Colors.white),
                  ),
                  pinned: true,
                ),
                SliverToBoxAdapter(
                  child: ExpansionSettingsTile(
                    initiallyExpanded: true,
                    title: tr('titles.gender'),
                    children: <Widget>[
                      CheckboxSettingsTile(
                        settingKey: 'gender.male',
                        title: tr('titles.male'),
                      ),
                      CheckboxSettingsTile(
                        settingKey: 'gender.female',
                        title: tr('titles.female'),
                      ),
                      CheckboxSettingsTile(
                        settingKey: 'gender.other',
                        title: tr('titles.other'),
                      )
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: ExpansionSettingsTile(
                    initiallyExpanded: true,
                    title: tr('titles.lifestyle'),
                    children: <Widget>[
                      CheckboxSettingsTile(
                        settingKey: 'lifestyle.vegan',
                        title: tr('titles.vegan'),
                      ),
                      CheckboxSettingsTile(
                        settingKey: 'lifestyle.vegetarian',
                        title: tr('titles.vegetarian'),
                      ),
                      CheckboxSettingsTile(
                        settingKey: 'lifestyle.interested',
                        title: tr('titles.interested'),
                      )
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: SettingsTileGroup(
                      title: 'Distance or country',
                      children: [
                        SwitchSettingsTile(
                          settingKey: 'byDistance',
                          title: 'Distance',
                          // icon: Icon(MdiIcons.mapMarkerDistance),
                          icon: Icon(MdiIcons.mapMarkerDistance),
                        ),
                        (pref != null)
                            ? SliderSettingsTile(
                                settingKey: 'distance',
                                title: 'Distancia en metros',
                                visibleIfKey: 'byDistance',
                                maxValue: 2000.0,
                                minValue: 100.0,
                                step: 100.0,
                                defaultValue:
                                    pref.getDouble('distance') ?? 100.0,
                                // minIcon: Icon(Icons.brightness_4),
                                // minIcon: Icon(MdiIcons.mapMarkerDistance),
                                // icon: Icon(MdiIcons.mapMarkerDistance),
                              )
                            : Container(),
                        // SwitchSettingsTile(
                        //   settingKey: 'byCountry',
                        //   title: 'Country and city',
                        //   // icon: Icon(MdiIcons.mapMarkerDistance),
                        //   icon: Icon(MdiIcons.mapMarkerDistance),
                        // ),
                      ]),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0),
                    child: Builder(
                      builder: (context) {
                        return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80.0)),
                          padding: EdgeInsets.all(0.0),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color.fromARGB(255, 255, 151, 71),
                                    Color.fromARGB(255, 252, 101, 114),
                                    // Color.fromARGB(255, 139, 157, 195),
                                  ],
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                ),
                                borderRadius: BorderRadius.circular(30.0)),
                            child: Container(
                              constraints: BoxConstraints(
                                maxWidth: 380.0,
                                minHeight: 50.0,
                              ),
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    MdiIcons.reload,
                                    color: Colors.white70,
                                  ),
                                  VerticalDivider(
                                    color: Colors.transparent,
                                  ),
                                  Text(
                                    "Actualizar tarjetas",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                            ),
                          ),
                          onPressed: () async {
                            await SharedFunctions.f.loadCards();
                            Get.back();
                          },
                        );
                      },
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }

  // Future<void> loadCards() async {
  //   print("Se piden 5 perfiles");
  //   Map response = await ApiProvider.api.getUsers();
  //   // print(response);
  //   List<Profile> temp = [];
  //   if (response['status'] == 'success') {
  //     for (var profile in response['users']) {
  //       int age = SharedFunctions.f.calculateAge(profile['bornDate']);
  //       if ((profile['image_users'] as List).isEmpty) continue;

  //       Profile newProfile = new Profile(
  //         idUser: profile['idUser'],
  //         names: profile['names'],
  //         surnames: profile['surnames'],
  //         city: profile['city'].toString(),
  //         country: profile['country'].toString(),
  //         age: age.toString(),
  //         description: profile['description'],
  //         gender: profile['gender'],
  //         interestedIn: profile['interestedIn'],
  //         interests: profile['interests'],
  //         job: profile['job'],
  //         lifeStyle: profile['lifeStyle'],
  //         location: profile['location'],
  //         lookingFor: profile['lookingFor'],
  //         userImages: profile['image_users'],
  //       );
  //       temp.add(newProfile);
  //     }
  //     print("${temp.length} perfiles descargados cardsPreferences");
  //     bloc.changeProfiles(temp);
  //   } else {}
  // }
}
