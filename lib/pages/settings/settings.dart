import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_select/smart_select.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/pages/blocs/socialpage_bloc.dart';
import 'package:vegwe/providers/db_provider.dart';
import 'package:vegwe/providers/socketIO_provider.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  List<SmartSelectOption<String>> localeOptions;
  Locale locale;
  String _language;
  SocialPageBloc bloc;

  @override
  void initState() {
    super.initState();
    localeOptions = [
      SmartSelectOption<String>(value: 'en_US', title: tr('locales.en')),
      SmartSelectOption<String>(value: 'es_ES', title: tr('locales.es')),
      SmartSelectOption<String>(value: 'pt_BR', title: tr('locales.pt')),
    ];
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = Provider.of(context);
    print(EasyLocalization.of(context).locale.toString());
  }

  void _logout() async {
    SocketIOProvider.io.endSOcketIO();
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.remove('api_token');
    pref.remove('idUser');
    pref.clear();
    DBProvider.db.destroyDb();
    bloc.clear();
    Navigator.pushNamedAndRemoveUntil(
      context,
      'login',
      ModalRoute.withName('login'),
    );
  }

  @override
  Widget build(BuildContext context) {
    // final bloc = Provider.of(context);

    if (_language == null) {
      locale = EasyLocalization.of(context).locale;
      setState(() {
        _language = locale.toString();
      });
      // print(_language);
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          backgroundColor: Colors.white,
          elevation: 5,
          // automaticallyImplyLeading: true,
          leading: IconButton(
            icon: FaIcon(FontAwesomeIcons.arrowLeft),
            color: Colors.grey,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: Text(
            tr('pages.settings'),
            style: Theme.of(context)
                .textTheme
                .headline4
                .copyWith(color: Colors.black87),
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(
              MdiIcons.accountEdit,
              color: Colors.orange,
              size: 30,
            ),
            title: Text(tr("settings.profileInformation")),
            onTap: () {
              Get.offNamed('userSettings');
            },
          ),
          ListTile(
            leading: Icon(
              MdiIcons.cards,
              color: Colors.cyan,
              size: 30,
            ),
            title: Text(tr("settings.cardsPreferences")),
            onTap: () {
              Get.offNamed('cardsPreferences');
            },
          ),
          ListTile(
            leading: FaIcon(
              FontAwesomeIcons.language,
              color: Colors.indigo,
            ),
            title: SmartSelect<String>.single(
                // choiceConfig: SmartSelectChoiceConfig(
                // ),
                modalType: SmartSelectModalType.bottomSheet,
                modalConfig: SmartSelectModalConfig(
                  useFilter: true,
                ),
                padding: EdgeInsets.all(0),
                title: tr("settings.language"),
                isTwoLine: true,
                value: _language,
                placeholder: tr("settings.selectLanguage"),
                options: localeOptions,
                onChange: (val) {
                  EasyLocalization.of(context).locale = localeFromString(val);
                  setState(() {
                    _language = val;
                  });
                }),
          ),
          ListTile(
            leading: Icon(
              MdiIcons.logout,
              color: Colors.blueGrey,
            ),
            title: Text(tr("settings.logout")),
            onTap: () {
              _logout();
            },
          ),
        ],
      ),
    );
  }
}
