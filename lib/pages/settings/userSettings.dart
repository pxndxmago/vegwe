import 'dart:convert';
import 'dart:math';
// import 'package:flutter_tr/flutter_tr.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:vegwe/providers/api_provider.dart';
import 'package:smart_select/smart_select.dart';
import 'package:extended_image/extended_image.dart';
import 'package:vegwe/pages/fragments/social/Gallery.dart';
import 'package:vegwe/SharedFunctions.dart';

class UserSettingsPage extends StatefulWidget {
  UserSettingsPage({Key key}) : super(key: key);

  @override
  _UserSettingsPageState createState() => _UserSettingsPageState();
}

class _UserSettingsPageState extends State<UserSettingsPage> {
  TextEditingController _namesController;
  TextEditingController _surnamesController;
  TextEditingController _occupationController;
  String _country;
  String _city;
  String _gender;
  String _lifeStyle;
  TextEditingController _descriptionController;
  List<String> _interests = [];
  List<String> _languages = [];

  List<SmartSelectOption<String>> countriesOptions = [];
  List<SmartSelectOption<String>> citiesOptions = [];
  List<SmartSelectOption<String>> interestsOptions = [];
  List<SmartSelectOption<String>> languagesOptions;
  // List<File> userImages;
  List<dynamic> userImages = [];

  dynamic _profileImage = Image.asset('assets/images/transparent.png');
  bool _isFilePickerButtonActive = true;
  bool interestsLoading = false;
  bool countriesLoading = false;
  bool citiesLoading = false;

  @override
  void initState() {
    super.initState();

    languagesOptions = [
      SmartSelectOption<String>(value: 'french', title: 'Frances'),
      SmartSelectOption<String>(value: 'spanish', title: 'English'),
      SmartSelectOption<String>(value: 'english', title: 'Ingles'),
      SmartSelectOption<String>(value: 'polish', title: 'Polaco'),
      SmartSelectOption<String>(value: 'german', title: 'Alemán'),
      SmartSelectOption<String>(value: 'chinese', title: 'Chino Mandarin'),
      SmartSelectOption<String>(value: 'japanese', title: 'Japonés'),
    ];
    _namesController = new TextEditingController();
    _surnamesController = new TextEditingController();
    _occupationController = new TextEditingController();
    _descriptionController = new TextEditingController();

    loadData();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await _getInterests();
    _getCountries();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: true,
        body: Builder(
          builder: (context) {
            return GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverAppBar(
                    actions: <Widget>[
                      FlatButton(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.check_circle,
                              color: Colors.white,
                            ),
                            VerticalDivider(
                              color: Colors.transparent,
                              width: 4,
                            ),
                            Text(
                              tr('menus.save'),
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle
                                  .copyWith(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () {
                          updateUser(context);
                        },
                      )
                    ],
                    expandedHeight: MediaQuery.of(context).size.height * 0.5,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: false,
                      title: Text(
                        tr('titles.profile'),
                        style: Theme.of(context)
                            .textTheme
                            .title
                            .copyWith(color: Colors.white),
                      ),
                      background: Container(
                        foregroundDecoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.transparent,
                              Colors.transparent,
                              Colors.black54
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                          ),
                        ),
                        // height: MediaQuery.of(context).size.height,
                        // width: MediaQuery.of(context).size.width,
                        child: _profileImage,
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: TextField(
                              controller: _namesController,
                              decoration: InputDecoration(
                                  labelText: tr('titles.names')),
                            ),
                          ),
                        ),
                        VerticalDivider(),
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: TextField(
                              controller: _surnamesController,
                              decoration: InputDecoration(
                                  labelText: tr('titles.surnames')),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: TextField(
                              controller: _occupationController,
                              decoration: InputDecoration(
                                  labelText: tr('titles.occupation')),
                            ),
                          ),
                        ),
                        VerticalDivider(),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Text(
                                    tr('titles.gender'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(color: Colors.lightBlue),
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: DropdownButton<String>(
                                    value: _gender,
                                    // icon: Icon(Icons.arrow_downward),
                                    // iconSize: 24,
                                    elevation: 16,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subhead
                                        .copyWith(),
                                    underline: Container(
                                        // height: 2,
                                        // color: Colors.deepPurpleAccent,
                                        ),
                                    onChanged: (String newValue) {
                                      setState(() {
                                        _gender = newValue;
                                      });
                                    },
                                    items: <List<String>>[
                                      ['female', tr('titles.female')],
                                      ['male', tr('titles.male')],
                                      ['other', tr('titles.other')]
                                    ].map((List<String> value) {
                                      return DropdownMenuItem<String>(
                                        value: value[0],
                                        child: Text(value[1]),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: SmartSelect<String>.single(
                              choiceConfig: SmartSelectChoiceConfig(
                                isGrouped: true,
                              ),
                              modalType: SmartSelectModalType.bottomSheet,
                              modalConfig: SmartSelectModalConfig(
                                useFilter: true,
                              ),
                              isLoading: countriesLoading,
                              padding: EdgeInsets.all(0),
                              title: tr('titles.country'),
                              isTwoLine: true,
                              value: _country,
                              placeholder: "Seleccione un pais",
                              options: countriesOptions,
                              onChange: (val) {
                                setState(() {
                                  _country = val;
                                  _city = null;
                                });
                                _getCities();
                              },
                            ),
                          ),
                        ),
                        VerticalDivider(),
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: SmartSelect<String>.single(
                                choiceConfig: SmartSelectChoiceConfig(
                                  isGrouped: true,
                                ),
                                modalType: SmartSelectModalType.bottomSheet,
                                modalConfig:
                                    SmartSelectModalConfig(useFilter: true),
                                padding: EdgeInsets.all(0),
                                title: tr('titles.city'),
                                isTwoLine: true,
                                value: _city,
                                isLoading: citiesLoading,
                                placeholder: "Seleccione una ciudad",
                                options: citiesOptions,
                                onChange: (val) {
                                  setState(() {
                                    _city = val;
                                  });
                                }),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Text(
                                    tr('titles.lifestyle'),
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(color: Colors.lightBlue),
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: DropdownButton<String>(
                                    value: _lifeStyle,
                                    // icon: Icon(Icons.arrow_downward),
                                    // iconSize: 24,
                                    elevation: 16,
                                    style: Theme.of(context)
                                        .textTheme
                                        .subhead
                                        .copyWith(),
                                    underline: Container(
                                        // height: 2,
                                        // color: Colors.deepPurpleAccent,
                                        ),
                                    onChanged: (String newValue) {
                                      setState(() {
                                        _lifeStyle = newValue;
                                      });
                                    },
                                    items: <List<String>>[
                                      ['vegan', tr('titles.vegan')],
                                      ['vegetarian', tr('titles.vegetarian')],
                                      ['interested', tr('titles.interested')]
                                    ].map((List<String> value) {
                                      return DropdownMenuItem<String>(
                                        value: value[0],
                                        child: Text(value[1]),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            tr('titles.description'),
                            style: Theme.of(context).textTheme.title.copyWith(
                                  color: Colors.orange,
                                  // fontWeight: FontWeight.bold,
                                ),
                          ),
                          Divider(color: Colors.transparent),
                          TextField(
                            controller: _descriptionController,
                            minLines: 5,
                            maxLines: 5,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(8),
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ],
                      ),
                      // Gallery()
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Divider(color: Colors.transparent),
                          SmartSelect<String>.multiple(
                            choiceConfig: SmartSelectChoiceConfig(
                              // isGrouped: true,
                              style: SmartSelectChoiceStyle(),
                              // secondaryBuilder: (context, item) => CircleAvatar(
                              //   backgroundColor: Colors.transparent,
                              //   foregroundColor: Colors.grey,
                              //   child: Icon(Icons.toys),
                              // ),
                            ),
                            builder: (context, state, showChoices) {
                              return ChipsTile(
                                title: tr('titles.interests'),
                                context: context,
                                titleStyle:
                                    Theme.of(context).textTheme.title.copyWith(
                                          color: Colors.orange,
                                          // fontWeight: FontWeight.bold,
                                        ),
                                state: state,
                                showChoices: showChoices,
                                onChipsDeleted: (value) {
                                  setState(() => _interests.remove(value));
                                },
                              );
                            },
                            modalType: SmartSelectModalType.bottomSheet,
                            modalConfig:
                                SmartSelectModalConfig(useFilter: true),
                            padding: EdgeInsets.all(0),
                            title: tr('titles.interests'),
                            isTwoLine: true,
                            choiceType: SmartSelectChoiceType.chips,
                            placeholder: "Seleccione sus intereses",
                            options: interestsOptions,
                            isLoading: interestsLoading,
                            onChange: (val) {
                              setState(() {
                                _interests = val;
                              });
                            },
                            value: _interests,
                          )
                        ],
                      ),
                      // Gallery()
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Divider(color: Colors.transparent),
                          SmartSelect<String>.multiple(
                            choiceConfig: SmartSelectChoiceConfig(
                              // isGrouped: true,
                              style: SmartSelectChoiceStyle(),
                              // secondaryBuilder: (context, item) => CircleAvatar(
                              //   backgroundColor: Colors.transparent,
                              //   foregroundColor: Colors.grey,
                              //   child: Icon(Icons.toys),
                              // ),
                            ),
                            builder: (context, state, showChoices) {
                              return ChipsTile(
                                title: tr('titles.languages'),
                                titleStyle:
                                    Theme.of(context).textTheme.title.copyWith(
                                          color: Colors.green,
                                        ),
                                context: context,
                                state: state,
                                showChoices: showChoices,
                                onChipsDeleted: (value) {
                                  setState(() => _languages.remove(value));
                                },
                              );
                            },
                            modalType: SmartSelectModalType.bottomSheet,
                            modalConfig:
                                SmartSelectModalConfig(useFilter: true),
                            padding: EdgeInsets.all(0),
                            title: tr('titles.languages'),
                            isTwoLine: true,
                            choiceType: SmartSelectChoiceType.chips,
                            placeholder: "Seleccione una ciudad",
                            options: languagesOptions,
                            onChange: (val) {
                              setState(() {
                                _languages = val;
                              });
                            },
                            value: _languages,
                          )
                        ],
                      ),
                      // Gallery()
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            tr('titles.photos'),
                            style: Theme.of(context).textTheme.title.copyWith(
                                  color: Colors.deepOrange,
                                ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.add_circle_outline,
                              color: Theme.of(context).indicatorColor,
                            ),
                            onPressed: (_isFilePickerButtonActive)
                                ? _pickImageAndUpload
                                : null,
                          ),
                        ],
                      ),
                      // Gallery()
                    ),
                  ),
                  SliverPadding(
                    padding: EdgeInsets.all(2.0),
                    sliver: SliverGrid(
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 160.0,
                        mainAxisSpacing: 2.0,
                        crossAxisSpacing: 2.0,
                        childAspectRatio: 1.0,
                      ),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          return GestureDetector(
                            onLongPress: () {
                              Get.bottomSheet(
                                Container(
                                  color: Colors.white,
                                  child: Wrap(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          // mainAxisAlignment:
                                          //     MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            ListTile(
                                              leading: Icon(
                                                MdiIcons.image,
                                                color: Colors.cyan,
                                              ),
                                              title: Text(
                                                  'Set as profile picture'),
                                              onTap: () {
                                                print("Hola $index");
                                              },
                                            ),
                                            ListTile(
                                              leading: Icon(
                                                MdiIcons.delete,
                                                color: Colors.deepOrange,
                                              ),
                                              title: Text('Delete picture'),
                                              onTap: () async {
                                                await deleteImage(index);
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              color: Colors.grey.shade900,
                              child: ExtendedImage.network(
                                "${ApiProvider.userImagesUrl}/${userImages[index]['idImage']}",
                                cache: true,
                                fit: BoxFit.cover,
                              ),
                            ),
                            onTap: () {
                              Navigator.pushNamed(context, 'gallery',
                                  arguments:
                                      GalleryArguments(userImages, index));
                            },
                          );
                        },
                        childCount: userImages.length,
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      height: 20,
                    ),
                  )
                ],
              ),
            );
          },
        ));
  }

  deleteImage(int index) async {
    Map resp =
        await ApiProvider.api.deleteUserImage(userImages[index]['idImage']);
    if (resp['status'] == 'success') {
      print('success');
      userImages.removeAt(index);
      if (mounted) setState(() {});
      // loadData();
      // Get.snackbar('Vegwe', 'Picture deleted');
    }
    Get.back();
  }

  updateUser(context) async {
    // print("Guardar pressed");
    Map responseObject = await ApiProvider.api.updateUser(
      names: _namesController.text,
      surnames: _surnamesController.text,
      bornDate: "",
      country: _country,
      city: _city,
      location: "",
      description: _descriptionController.text,
      interests: _interests.join(','),
      languages: _languages.join(','),
      lifeStyle: _lifeStyle,
      lookingFor: "",
      interestedIn: "",
      gender: _gender,
      job: _occupationController.text,
    );
    if (responseObject['status'] == 'success') {
      // Get.snackbar(
      //   'Vegwe',
      //   "Usuario actualizado",
      //   snackPosition: SnackPosition.BOTTOM,
      //   margin: EdgeInsets.all(0),
      //   snackStyle: SnackStyle.GROUNDED,
      //   borderRadius: 0,
      //   overlayBlur: 0,
      //   barBlur: 80,
      //   backgroundColor: Colors.black45,
      //   colorText: Colors.white70
      // );
      SharedFunctions.f.showSnackbar('Vegwe', "Usuario actualizado");
    } else {
      _showNotification(context, "Error al actualizar el usuario");
    }
  }

  loadUserImages(Map responseObject) async {
    userImages.clear();
    userImages = responseObject['user']['image_users'];
    if (userImages.isNotEmpty) {
      _profileImage = ExtendedImage.network(
        "${ApiProvider.userImagesUrl}/${userImages[0]['idImage']}",
        cache: true,
        fit: BoxFit.cover,
      );
    }
  }

  loadData() async {
    try {
      Map responseObject = await ApiProvider.api.getUser();
      // print(json.encode(responseObject));

      if (responseObject['status'] == 'success') {
        loadUserImages(responseObject);
        _namesController.text = responseObject['user']['names'] ?? "";
        _surnamesController.text = responseObject['user']['surnames'] ?? "";
        _occupationController.text = responseObject['user']['bornDate'] ?? "";
        _gender = responseObject['user']['gender'] ?? "";
        _lifeStyle = responseObject['user']['lifeStyle'] ?? "";
        _descriptionController.text =
            responseObject['user']['description'] ?? "";
        _occupationController.text = responseObject['user']['job'] ?? "";
        _interests =
            responseObject['user']['interests'].toString().split(',') ?? [];
        _languages =
            responseObject['user']['languages'].toString().split(',') ?? [];
        _country = responseObject['user']['country'].toString() ?? "";
        if (_country != '' && int.parse(_country) > 0) _getCities();
        _city = responseObject['user']['city'].toString() ?? "";
        setState(() {});
      } else {
        print(responseObject);
        Navigator.of(context).pop();
        // _showNotification(context, "Error al cargar los datos");
      }
    } catch (e) {
      print('Error en metodo loadData: ');
      print(e);
    }
  }

  _showNotification(BuildContext context, String text) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
      ),
    );
  }

  _pickImageAndUpload() async {
    _isFilePickerButtonActive = false;
    try {
      var file = await ImagePicker.pickImage(
        source: ImageSource.gallery,
      );
      if (file != null) {
        http.StreamedResponse response =
            await ApiProvider.api.uploadUserImage(file, userImages.isEmpty);
        if (response.statusCode == 200) {
          if (userImages.isEmpty) {
            setState(() {
              _profileImage = Image(
                image: FileImage(file),
                // image: AssetImage('assets/profile.jpg'),
                fit: BoxFit.cover,
              );
            });
          }
          Map responseBody = json.decode(await response.stream.bytesToString());
          print(responseBody);
          Map image = {
            "idImage": responseBody['image']['idImage'],
            "principal": responseBody['image']['principal'],
            "idUser": responseBody['image']['idUser']
          };

          userImages.add(image);
          setState(() {});
        }
      }
    } catch (e) {
      print("error en _pickImageAndUpload method");
      print(e);
    }
    _isFilePickerButtonActive = true;
  }

  _getInterests() async {
    if (mounted) setState(() => interestsLoading = true);
    Map resp = await ApiProvider.api.getInterests(context);
    if (resp['status'] == 'success') {
      for (var interest in resp['interests']) {
        interestsOptions.add(SmartSelectOption<String>(
            value: interest['cod'], title: interest['text']));
      }
    }
    if (mounted) setState(() => interestsLoading = false);
  }

  _getCountries() async {
    if (mounted) setState(() => countriesLoading = true);

    Map resp = await ApiProvider.api.getCountries();
    if (resp['status'] == 'success') {
      countriesOptions.clear();
      for (var country in resp['countries']) {
        countriesOptions.add(
          SmartSelectOption<String>(
              value: country['id'].toString(),
              title:
                  "${country['emoji']} ${country['name']} - ${country['native']}",
              group: "America"),
        );
      }
    }
    if (mounted) setState(() => countriesLoading = false);
  }

  _getCities() async {
    if (mounted) setState(() => citiesLoading = true);

    Map resp = await ApiProvider.api.getCities(int.parse(_country));
    if (resp['status'] == 'success') {
      citiesOptions.clear();
      for (var city in resp['cities']) {
        citiesOptions.add(
          SmartSelectOption<String>(
              value: city['id'].toString(),
              title: city['name'],
              group: "America"),
        );
      }
    }
    if (mounted) setState(() => citiesLoading = false);
  }
}

class ChipsTile<T> extends StatelessWidget {
  final String title;
  final SmartSelectState<T> state;
  final SmartSelectShowModal showChoices;
  final Function(T value) onChipsDeleted;
  final BuildContext context;
  final titleStyle;
  ChipsTile({
    Key key,
    this.title,
    this.state,
    this.showChoices,
    this.onChipsDeleted,
    this.context,
    this.titleStyle,
  }) : super(key: key);
  List<Color> colors = [
    Colors.blueAccent,
    Colors.redAccent,
    Colors.pink,
    Colors.lightBlue,
    Colors.cyan.shade400,
    Colors.redAccent,
    Colors.pink,
    Colors.deepPurple,
    Colors.purple
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListTile(
          contentPadding: EdgeInsets.all(0),
          title: Text(
            title,
            style: titleStyle,
          ),
          trailing: IconButton(
            icon: Icon(
              Icons.add_circle_outline,
              color: Theme.of(context).indicatorColor,
            ),
            onPressed: () => showChoices(context),
          ),
        ),
        Divider(height: 1),
        _chips,
      ],
    );
  }

  Widget get _chips {
    return state.valuesObject?.isNotEmpty ?? false
        ? Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 5,
            ),
            child: Wrap(
              runAlignment: WrapAlignment.start,
              alignment: WrapAlignment.start,
              spacing: 5,
              runSpacing: 0,
              children: List<Widget>.generate(
                state.valuesObject.length,
                (i) {
                  Color randomColor = colors[Random().nextInt(colors.length)];
                  return Chip(
                    label: Text(state.valuesObject[i].title),
                    labelStyle: Theme.of(context).textTheme.body1.copyWith(
                        color: randomColor.computeLuminance() > 0.3
                            ? Colors.black87
                            : Colors.white),
                    backgroundColor: randomColor,
                    shape: StadiumBorder(
                      side: BorderSide(
                        color: randomColor,
                      ),
                    ),
                    onDeleted: () {
                      onChipsDeleted?.call(state.valuesObject[i].value);
                    },
                  );
                },
              ).toList(),
            ),
          )
        : Container(
            padding: EdgeInsets.all(25),
            child: Center(
              child: Text(state.placeholder),
            ),
          );
  }
}
