import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:vegwe/providers/api_provider.dart';
import 'package:video_player/video_player.dart';
import 'dart:ui' as ui;
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:geolocator/geolocator.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:vegwe/SharedFunctions.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _obscureText = true;
  bool _isLoadingButton = false;
  VideoPlayerController _videoController;
  Position currentPosition;
  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: [
    'email',
    'profile',
    'https://www.googleapis.com/auth/user.birthday.read',
    'https://www.googleapis.com/auth/user.gender.read'
  ]);

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _loginFormKey = GlobalKey<FormState>();
  final FacebookLogin facebookLogin = FacebookLogin();
  TextEditingController _correoController = new TextEditingController();
  TextEditingController _contrasenaController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    _videoController = VideoPlayerController.asset('assets/letsvegvideo.mp4')
      ..initialize().then((_) {
        _videoController.play();
        _videoController.setLooping(true);
        // Ensure the first frame is shown after the video is initialized
        setState(() {});
      });
    _checkIsLogin(context);

    // facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    facebookLogin.loginBehavior = FacebookLoginBehavior.nativeOnly;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          SizedBox.expand(
            child: FittedBox(
              fit: BoxFit.cover,
              child: SizedBox(
                  width: _videoController.value.size?.width ?? 0,
                  height: _videoController.value.size?.height ?? 0,
                  child: VideoPlayer(_videoController)),
              // child: Image.asset('assets/images/login.jpg')),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              // width: MediaQuery.of(context).size.width * 0.8,
              // margin: EdgeInsets.all(25),
              padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
              child: ClipRect(
                child: Container(
                  padding: EdgeInsets.all(20),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: BackdropFilter(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        _buildLogoContainer(),
                        Divider(
                          color: Colors.transparent,
                        ),
                        _getLoginForm(),
                      ],
                    ),
                    filter: ui.ImageFilter.blur(
                      sigmaX: 5.0,
                      sigmaY: 5.0,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLogoContainer() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.bottomCenter,
            height: 120,
            width: 120,
            child: Image(
              image: AssetImage('assets/logo.png'),
            ),
          ),
          Text(
            "VegWe",
            style: TextStyle(
              fontFamily: 'Rochester',
              fontSize: 50,
              fontWeight: FontWeight.bold,
              letterSpacing: -2,
              foreground: Paint()
                ..shader = ui.Gradient.linear(
                  Offset(0, 50),
                  Offset(300, 20),
                  <Color>[
                    Color.fromARGB(255, 257, 181, 74),
                    Color.fromARGB(255, 220, 234, 33),
                  ],
                ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getLoginForm() {
    return Expanded(
      flex: 1,
      child: Form(
        key: _loginFormKey,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    controller: _correoController,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    cursorColor: Colors.white,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        hintText: tr('titles.email'),
                        hintStyle: TextStyle(color: Colors.white54),
                        icon: FaIcon(
                          FontAwesomeIcons.userAlt,
                          size: 20,
                        ),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: BorderSide(color: Colors.transparent)),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                        filled: true,
                        fillColor: Colors.black38),
                  ),
                  Divider(
                    color: Colors.transparent,
                    height: 25,
                  ),
                  TextFormField(
                    controller: _contrasenaController,
                    obscureText: _obscureText,
                    cursorColor: Colors.white,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        hintText: tr('titles.password'),
                        hintStyle: TextStyle(color: Colors.white54),
                        contentPadding: EdgeInsets.only(
                            top: 0, bottom: 15, left: 15, right: 0),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: BorderSide(color: Colors.transparent),
                        ),
                        icon: FaIcon(
                          FontAwesomeIcons.key,
                          size: 20,
                        ),
                        suffix: IconButton(
                          onPressed: () {
                            setState(() {
                              print('Obscure');
                              _obscureText = !_obscureText;
                            });
                          },
                          icon: FaIcon(
                            _obscureText
                                ? FontAwesomeIcons.eyeSlash
                                : FontAwesomeIcons.eye,
                            size: 20,
                            color: Colors.white70,
                          ),
                        ),
                        filled: true,
                        fillColor: Colors.black38),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Builder(
                    builder: (context) {
                      return RaisedButton(
                        onPressed: () {
                          _login(context);
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        padding: EdgeInsets.all(0.0),
                        child: Ink(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromARGB(255, 257, 181, 74),
                                  Color.fromARGB(255, 220, 234, 33),
                                ],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                              borderRadius: BorderRadius.circular(30.0)),
                          child: Container(
                            constraints: BoxConstraints(
                              maxWidth: 300.0,
                              minHeight: 50.0,
                            ),
                            alignment: Alignment.center,
                            child: _isLoadingButton
                                ? CircularProgressIndicator()
                                : Text(
                                    tr("buttons.login"),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  ),
                          ),
                        ),
                      );
                    },
                  ),
                  Divider(
                    color: Colors.transparent,
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Text(
                      tr('messages.forgotPassord'),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Divider(
                    color: Colors.transparent,
                    height: 20,
                  ),
                  Builder(
                    builder: (context) {
                      return RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        padding: EdgeInsets.all(0.0),
                        child: Ink(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromARGB(255, 6, 119, 229),
                                  Color.fromARGB(255, 59, 89, 152),
                                  // Color.fromARGB(255, 139, 157, 195),
                                ],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                              borderRadius: BorderRadius.circular(30.0)),
                          child: Container(
                            constraints: BoxConstraints(
                              maxWidth: 300.0,
                              minHeight: 50.0,
                            ),
                            alignment: Alignment.center,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FaIcon(
                                  FontAwesomeIcons.facebookF,
                                  color: Colors.white70,
                                ),
                                VerticalDivider(
                                  color: Colors.transparent,
                                ),
                                Text(
                                  "Iniciar sesión con Facebook",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18),
                                )
                              ],
                            ),
                          ),
                        ),
                        onPressed: () async {
                          loginWithFacebook(context);
                        },
                      );
                    },
                  ),
                  Divider(
                    color: Colors.transparent,
                    height: 20,
                  ),
                  Builder(
                    builder: (context) {
                      return RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(80.0)),
                        padding: EdgeInsets.all(0.0),
                        child: Ink(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromARGB(255, 203, 64, 35),
                                  Color.fromARGB(255, 250, 64, 35),
                                  // Color.fromARGB(255, 139, 157, 195),
                                ],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                              borderRadius: BorderRadius.circular(30.0)),
                          child: Container(
                            constraints: BoxConstraints(
                              maxWidth: 300.0,
                              minHeight: 50.0,
                            ),
                            alignment: Alignment.center,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FaIcon(
                                  FontAwesomeIcons.google,
                                  color: Colors.white70,
                                ),
                                VerticalDivider(
                                  color: Colors.transparent,
                                ),
                                Text(
                                  "Iniciar sesión con Google",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18),
                                )
                              ],
                            ),
                          ),
                        ),
                        onPressed: () async {
                          try {
                            loginWithGoogle(context);
                          } catch (error) {
                            print(error);
                          }
                        },
                      );
                    },
                  ),
                  Divider(
                    color: Colors.transparent,
                    height: 25,
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: GestureDetector(
                      child: Text(
                        "Si no tienes una cuenta, toca aqui 😁",
                        style: TextStyle(color: Colors.white),
                      ),
                      onTap: () {
                        Get.toNamed("signUp");

                        // Navigator.pushNamed(context, 'signUp');
                      },
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Colors.transparent,
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  void _login(BuildContext context) async {
    setState(() {
      _isLoadingButton = true;
    });

    String _email = _correoController.text.trim();
    String _password = _contrasenaController.text.trim();

    try {
      Map resp;
      if (currentPosition != null) {
        resp = await ApiProvider.api.login(
          _email,
          _password,
          longitude: currentPosition.longitude,
          latitude: currentPosition.latitude,
        );
      } else {
        resp = await ApiProvider.api.login(_email, _password);
      }

      if (resp['status'] == 'success') {
        _correoController.clear();
        _contrasenaController.clear();
        Get.toNamed("social", arguments: 'Get is the best');
        print('credenciales correctas');
      } else {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Text(resp['error']),
          ),
        );
      }
    } catch (e) {
      print("Error on _login method: $e");
    }
    if (mounted) {
      setState(() {
        _isLoadingButton = false;
      });
    }
  }

  void loginWithFacebook(BuildContext context) async {
    await facebookLogin.logOut();
    final result =
        await facebookLogin.logIn(['email', 'user_birthday', 'user_gender']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        print(token);
        Map loginFBResp;
        if (currentPosition != null) {
          loginFBResp = await ApiProvider.api.loginWithFacebook(
            token,
            longitude: currentPosition.longitude,
            latitude: currentPosition.latitude,
          );
        } else {
          loginFBResp = await ApiProvider.api.loginWithFacebook(token);
        }
        if (loginFBResp['status'] == 'success') {
          _correoController.clear();
          _contrasenaController.clear();
          SharedFunctions.f.showSnackbar('Vegwe', loginFBResp['message']);
          Get.toNamed("social");
          // Navigator.of(context)
          //     .pushNamedAndRemoveUntil('social', ModalRoute.withName('login'));
          // print('credenciales correctas');
        } else {
          SharedFunctions.f.showSnackbar('Vegwe', loginFBResp['error']);
        }
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("cancelledByUser");

        // _showCancelledMessage();
        break;
      case FacebookLoginStatus.error:
        print("error");
        print(result.errorMessage);

        // _showErrorOnUI(result.errorMessage);
        break;
    }
  }

  void loginWithGoogle(BuildContext context) async {
    await _googleSignIn.signOut();

    try {
      await _googleSignIn.signIn();
    } catch (e) {
      print(e);
    }
    Map headers;
    if (!await _googleSignIn.isSignedIn()) return;

    headers = await _googleSignIn.currentUser?.authHeaders;
    String token = headers['Authorization'].toString().split(' ')[1];
    print(token);
    Map loginGoogleResp;
    if (currentPosition != null) {
      loginGoogleResp = await ApiProvider.api.loginWithGoogle(
        token,
        longitude: currentPosition.longitude,
        latitude: currentPosition.latitude,
      );
    } else {
      loginGoogleResp = await ApiProvider.api.loginWithGoogle(token);
    }
    if (loginGoogleResp['status'] == 'success') {
      _correoController.clear();
      _contrasenaController.clear();
      SharedFunctions.f.showSnackbar('Vegwe', loginGoogleResp['message']);

      Get.toNamed("social");
      // Navigator.of(context)
      //     .pushNamedAndRemoveUntil('social', ModalRoute.withName('login'));
      print('credenciales correctas');
    } else {
      SharedFunctions.f.showSnackbar('Vegwe', loginGoogleResp['error']);
    }
  }

  Future<Null> _checkIsLogin(BuildContext context) async {
    if (await Geolocator().checkGeolocationPermissionStatus() ==
        GeolocationStatus.granted) {
      Position position = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      currentPosition = position;
      if (mounted) setState(() {});
      print(position);
      // List<Placemark> placemark =
      //     await Geolocator().placemarkFromPosition(position);
      // for (var place in placemark) {
      //   print(place.country);
      //   print(place.locality);
      //   print(place.thoroughfare);
      //   print(place.isoCountryCode);
      // }
    } else {
      if (!await Permission.location.isPermanentlyDenied)
        showDialog(
          context: context,
          builder: (_) => FlareGiffyDialog(
            flarePath: 'assets/vegwe.flr',
            flareAnimation: 'loop',
            title: Text(
              'Permisos',
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
            description: Text(
                'Necesitamos los permisos de ubicacion para brindarte un mejor servicio. Deseas otorgar servicios de ubicacion?'),
            entryAnimation: EntryAnimation.TOP,
            onOkButtonPressed: () async {
              Get.back();
              if (await Permission.location.request().isGranted) {
                Position position = await Geolocator()
                    .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
                print(position);
                currentPosition = position;
                if (mounted) setState(() {});
              }
            },
            onCancelButtonPressed: () async {
              Get.back();
              print("No se puede obtener la ubication");
            },
          ),
        );
      print("Fuera del showdialog");
    }
  }
}
