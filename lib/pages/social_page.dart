import 'package:admob_flutter/admob_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:minimize_app/minimize_app.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/pages/fragments/social/Notifications.dart';
import 'package:vegwe/pages/fragments/social/Cards.dart';
import 'package:vegwe/providers/api_provider.dart';
import 'package:vegwe/providers/socketIO_provider.dart';
import 'dart:ui' as ui;
import 'package:vegwe/providers/AdMobService.dart';
import 'package:vegwe/SharedFunctions.dart';

class SocialPage extends StatefulWidget {
  SocialPage({Key key}) : super(key: key);

  @override
  _SocialPageState createState() => _SocialPageState();
}

class _SocialPageState extends State<SocialPage> {
  // final FocusNode _focusNode = FocusNode();
  bool dialVisible = true;
  SocketIO socketIO;
  TextEditingController inputMsg;
  // ScrollController _scrollController = new ScrollController();
  String randomID = '';
  int conn = 0;
  var pagesContainer;
  bool _showTextLogo = true;
  int pageIndexS = 0;
  SocialPageBloc bloc;
  // String bodyFragment = "";
  AdmobInterstitial interstitialAd;
  @override
  void initState() {
    print("initState de social_page ejecutado");
    super.initState();
    inputMsg = new TextEditingController();
    interstitialAd = AdmobInterstitial(
      adUnitId: AdMobService.admob.getIntersticialId(),
      listener: (AdmobAdEvent event, Map<String, dynamic> args) {
        if (event == AdmobAdEvent.loaded) interstitialAd.show();
        if (event == AdmobAdEvent.failedToLoad) {
          // Start hoping they didn't just ban your account :)
          print("Error code: ${args['errorCode']}");
        }
      },
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc ??= Provider.of(context);
    _socketIOTask();
    SharedFunctions.f.context = context;
    _getChats();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _getChats() async {
    Map responseObject = await ApiProvider.api.getChats();
    if (responseObject['status'] == "success") {
      Provider.of(context).changeChats(responseObject['chats']);
      // print(Provider.of(context).chats);
      setState(() {});
    }
  }

  _socketIOTask() async {
    SocketIOProvider.io.context = context;
    SocketIOProvider.io.socketIO();
  }

  @override
  Widget build(BuildContext context) {
    // initSocketIO(bloc);
    return WillPopScope(
      onWillPop: () {
        MinimizeApp.minimizeApp();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        // extendBody: true,
        // extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: AppBar(
            backgroundColor: Colors.white,
            elevation: 5,
            flexibleSpace: _showTextLogo
                ? GestureDetector(
                    onTap: () async {
                      interstitialAd.load();
                    },
                    onDoubleTap: () {
                      Get.bottomSheet(Container(
                        color: Colors.white,
                        child: Wrap(
                          children: <Widget>[
                            ListTile(
                              leading: Icon(Icons.music_note),
                              onTap: () {},
                            )
                          ],
                        ),
                      ));
                    },
                    child: Center(
                      child: _buildLogoText(),
                    ),
                  )
                : Container(),
            automaticallyImplyLeading: false,
            // leading: Container(),
            centerTitle: true,
            title: Builder(builder: (context) {
              // print("Bodyfragment: " + bodyFragment);
              Widget title = Text('');
              // if (bodyFragment != null)
              switch (pageIndexS) {
                // case "cards":
                //   title = _buildLogoText();
                //   break;
                case 1:
                  title = Text(
                    tr('pages.notifications'),
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(color: Colors.black87),
                  );
                  break;
                case 2:
                  title = Text(
                    tr('pages.messages'),
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(color: Colors.black87),
                  );
                  break;
              }
              return title;
            }),
          ),
        ),
        body: IndexedStack(
          index: pageIndexS,
          children: <Widget>[
            CardsFragment(),
            // Center(child: Text("Cards desabilitado"),),
            NotificationsPage(),
            // MessagesFragment(),
          ],
        ),

        bottomNavigationBar: _buildBottomNavigationBar(bloc),
      ),
    );
  }

  void setDialVisible(bool value) {
    setState(() {
      dialVisible = value;
    });
  }

  _goToPage(SocialPageBloc bloc, String fragment, bool animate) {
    bloc.changeBodyFragment(fragment);
    int pageIndex;
    switch (fragment) {
      case 'cards':
        _showTextLogo = true;
        pageIndex = 0;
        setState(() {});

        break;
      case 'notifications':
        pageIndex = 1;
        _showTextLogo = false;
        setState(() {});

        break;
      case 'messages':
        pageIndex = 2;
        _showTextLogo = false;
        setState(() {});

        break;
      default:
    }
    setState(() {
      pageIndexS = pageIndex;
    });
    // if (animate) pageController?.jumpToPage(pageIndex);
    // pagesContainer?.animateToPage(pageIndex,
    //     duration: Duration(milliseconds: 300), curve: Curves.linear);
  }

  // Botton bar
  BottomAppBar _buildBottomNavigationBar(SocialPageBloc bloc) {
    final double _bottonIconPadding = 0.0;
    // final double _bottonIconSize = 22.0;
    final double _bottonIconSize = Get.height * 0.035;
    return BottomAppBar(
      color: Colors.white,
      child: Container(
          child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Builder(
            builder: (context) {
              bool _selected = pageIndexS != null && pageIndexS == 0;
              return Container(
                padding: EdgeInsets.only(bottom: _bottonIconPadding),
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 5,
                      color: _selected ? Colors.redAccent : Colors.transparent,
                    ),
                  ),
                ),
                child: IconButton(
                  padding: EdgeInsets.all(0),
                  tooltip: 'cards',
                  onPressed: () {
                    _goToPage(bloc, 'cards', true);
                  },
                  iconSize: _bottonIconSize + (_selected ? 4 : 0),
                  icon: Icon(
                    _selected ? MdiIcons.cardsOutline : MdiIcons.cards,
                    color: Colors.cyan,
                    size: _bottonIconSize,
                  ),
                ),
              );
            },
          ),
          Builder(
            builder: (context) {
              bool _selected = pageIndexS != null && pageIndexS == 1;
              return Container(
                padding: EdgeInsets.only(bottom: _bottonIconPadding),
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 5,
                      color: _selected ? Colors.redAccent : Colors.transparent,
                    ),
                  ),
                ),
                child: IconButton(
                  padding: EdgeInsets.all(0),
                  tooltip: 'notifications',
                  onPressed: () {
                    _goToPage(bloc, 'notifications', true);
                  },
                  iconSize: _bottonIconSize + (_selected ? 4 : 0),
                  icon: Icon(
                    _selected
                        ? MdiIcons.mailboxOpenUpOutline
                        : MdiIcons.mailbox,
                    size: _bottonIconSize,
                  ),
                  color: Colors.redAccent.shade100,
                ),
              );
            },
          ),
          Builder(
            builder: (context) {
              bool _selected = pageIndexS != null && pageIndexS == 2;
              return Container(
                padding: EdgeInsets.only(bottom: _bottonIconPadding),
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 5,
                      color: _selected ? Colors.redAccent : Colors.transparent,
                    ),
                  ),
                ),
                child: IconButton(
                  padding: EdgeInsets.all(0),
                  tooltip: 'Profile',
                  onPressed: () {
                    Navigator.pushNamed(context, 'settings');
                    // _goToPage(bloc, 'messages', true);
                  },
                  iconSize: _bottonIconSize + (_selected ? 4 : 0),
                  icon: Icon(
                    _selected ? MdiIcons.accountOutline : MdiIcons.account,
                    color: Colors.orange,
                    size: _bottonIconSize,
                  ),
                ),
              );
            },
          ),

          // SizedBox(
          //   width: 70.0,
          // ),
        ],
      )),
    );
  }

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  List<ChatMessage> listChat = new List<ChatMessage>();
}

_buildLogoText() {
  return Padding(
    padding: const EdgeInsets.only(top: 15.0),
    child: Text(
      "VegWe",
      style: GoogleFonts.rochester(
        fontSize: 50,
        fontWeight: FontWeight.bold,
        letterSpacing: -2,
        foreground: Paint()
          ..shader = ui.Gradient.linear(
            Offset(0, 50),
            Offset(300, 20),
            <Color>[
              Color.fromARGB(255, 257, 181, 74),
              Color.fromARGB(255, 220, 234, 33),
            ],
          ),
      ),
    ),
  );
}

class ChatMessage {
  String date, msg, id;
  ChatMessage({this.date, this.msg, this.id});
}
