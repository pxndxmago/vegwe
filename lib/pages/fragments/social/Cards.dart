import 'package:flutter/material.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/pages/fragments/custom_swipe_stack.dart';
import 'dart:async';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_image/extended_image.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:vegwe/SharedFunctions.dart';
import 'package:vegwe/pages/fragments/social/Profile.dart';
import 'package:vegwe/providers/api_provider.dart';
import 'package:vegwe/providers/AdMobService.dart';
// -------------------------------------------------
// import 'package:swipe_stack/swipe_stack.dart';

class CardsFragment extends StatefulWidget {
  CardsFragment({Key key}) : super(key: key);

  @override
  _CardsFragmentState createState() => _CardsFragmentState();
}

class _CardsFragmentState extends State<CardsFragment>
// with AutomaticKeepAliveClientMixin {
{
  final GlobalKey<SwipeStackState> _swipeKey = GlobalKey<SwipeStackState>();
  SocialPageBloc bloc;

  List<Map<String, dynamic>> historyCards;
  List<Profile> historyProfiles = [];
  List<Profile> profilesBack = [];
  List<Profile> profiles = [];
  Widget content;

  @override
  Widget build(BuildContext context) {
    if (content == null) {
      content = SwipeCard();
    }

    return content;
  }

  // @override
  // // TODO: implement wantKeepAlive
  // bool get wantKeepAlive => true;
}



class SwipeCard extends StatefulWidget {
  SwipeCard({Key key}) : super(key: key);
  @override
  _SwipeCardState createState() => _SwipeCardState();
}

class _SwipeCardState extends State<SwipeCard> {
  final GlobalKey<SwipeStackState> _swipeKey = GlobalKey<SwipeStackState>();
  SocialPageBloc bloc;
  List<Profile> profiles = [];
  StreamSubscription listener;
  @override
  void initState() {
    super.initState();
    Admob.initialize(testDeviceIds: ['ca-app-pub-1778889728876239~9610005056']);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    print('didChangeDependencies swipeCard');
    bloc = Provider.of(context);
    listener?.cancel();
    listener = bloc.profilesStream.listen((data) {
      profiles = data;
      print("Cantidad de perfiles descargados ${profiles.length}");
      print('Se detecta un cambio en profilesStream');
      if (mounted) setState(() {});
    });
    SharedFunctions.f.loadCards();
  }

  @override
  Widget build(BuildContext context) {
    print('Build de swipecard');

    return Container(
      padding: EdgeInsets.only(top: 15, bottom: 5, left: 15, right: 15),
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 12,
            child: SwipeStack(
              threshold: 15,
              historyCount: 5,
              maxAngle: 1,
              padding: EdgeInsets.all(0),
              visibleCount: 3,
              animationDuration: Duration(milliseconds: 200),
              stackFrom: StackFrom.None,
              translationInterval: 50,
              scaleInterval: 0.02,
              key: _swipeKey,
              children: profiles.map((Profile profile) {
                return new SwiperItem(
                    itemData: {
                      'profile': profile,
                    },
                    builder: (SwiperPosition position, double progress) {
                      return Card(
                        elevation: 6,
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: GestureDetector(
                          onTap: () {
                            Get.toNamed('profile',
                                arguments: ProfileArguments(profile.idUser));
                          },
                          child: Container(
                            child: Stack(
                              // mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: MediaQuery.of(context).size.height,
                                  width: MediaQuery.of(context).size.width,
                                  child: ExtendedImage.network(
                                    "${ApiProvider.userImagesUrl}/${profile.userImages[0]['idImage']}",
                                    cache: true,
                                    fit: BoxFit.cover,
                                    // "https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-9/95567558_10157636956224023_7282015560290795520_n.jpg?_nc_cat=110&_nc_sid=110474&_nc_ohc=Do7SlGUPQawAX-5qDgA&_nc_ht=scontent.fgye1-1.fna&oh=bb6565ed7c20f4a9671815c572b845fc&oe=5ED2159C",
                                  ),
                                ),
                                // _getFullScreenCarousel(
                                //     context, profile.getPhotos, true),
                                _getProfileInformation(profile),
                                _getIconPosition(position.index)
                              ],
                            ),
                          ),
                        ),
                      );
                    });
              }).toList(),
              onEnd: () {
                SharedFunctions.f.loadCards();
              },
              onSwipe: (int index, SwiperPosition position,
                  Map<String, dynamic> itemData) async {
                print(
                    '${itemData['profile'].idUser} ${itemData['profile'].names}');
                int like;
                if (SwiperPosition.Left == position) {
                  like = 0;
                } else {
                  like = 1;
                }
                Map response = await ApiProvider.api
                    .sendLike(itemData['profile'].idUser, like);
                if (response['status'] == 'fail') {
                  _swipeKey.currentState.rewind();
                }
                // historyCards = _swipeKey.currentState.history;
                // try {
                //   historyProfiles.add(profilesBack.last);
                //   profilesBack.removeLast();
                // } catch (e) {
                //   print(e);
                // }
              },
              onRewind: (int index, SwiperPosition position,
                  Map<String, dynamic> itemData) {
                // historyProfiles.add(itemData['profile']);
                // profilesBack.add(historyProfiles.last);
                // historyProfiles.removeLast();

                debugPrint("onRewind $index $position");
              },
            ),
          ),
          Visibility(
            visible: true,
            child: Flexible(
              fit: FlexFit.loose,
              child: AdmobBanner(
                  adUnitId: AdMobService.admob.getBannerId(),
                  adSize: AdmobBannerSize.BANNER,
                  listener: (AdmobAdEvent event, Map<String, dynamic> args) {
                    switch (event) {
                      case AdmobAdEvent.loaded:
                        break;

                      case AdmobAdEvent.opened:
                        print('Admob banner opened!');
                        break;

                      case AdmobAdEvent.closed:
                        print('Admob banner closed!');
                        break;

                      case AdmobAdEvent.failedToLoad:
                        print(
                            'Admob banner failed to load. Error code: ${args['errorCode']}');
                        break;
                      case AdmobAdEvent.clicked:
                        // TODO: Handle this case.
                        break;
                    }
                  }),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getIconPosition(int position) {
    if (position == 1) {
      return Padding(
        padding: const EdgeInsets.all(30.0),
        child: Align(
          alignment: Alignment.topRight,
          child: FaIcon(
            FontAwesomeIcons.thumbsDown,
            color: Colors.blue,
            size: 80,
          ),
        ),
      );
    } else if (position == 2) {
      return Padding(
        padding: const EdgeInsets.all(30.0),
        child: Align(
            alignment: Alignment.topLeft,
            child: FaIcon(
              FontAwesomeIcons.thumbsUp,
              color: Colors.green,
              size: 80,
            )),
      );
    } else {
      return SizedBox();
    }
  }

  // Muestra un la informacion del perfil
  Widget _getProfileInformation(Profile profile) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.transparent, Colors.black54],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                // crossAxisAlignment: CrossAxisAlignment.start,
                child: Row(
                  children: <Widget>[
                    Icon(
                      profile.gender == 'male'
                          ? MdiIcons.genderMale
                          : MdiIcons.genderFemale,
                      color:
                          profile.gender == 'male' ? Colors.cyan : Colors.pink,
                      size: 20,
                    ),
                    Text(
                      profile.names +
                          ", " +
                          profile.age.toString() +
                          " " +
                          profile.idUser.toString(),
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(color: Colors.white60),
                    ),
                  ],
                ),
              ),
              Expanded(
                // flex: 1,
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          height: 35,
                          width: 35,
                          child: FittedBox(
                            child: FloatingActionButton(
                              heroTag: "${profile.surnames}11",
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.orange,
                              child: FaIcon(FontAwesomeIcons.undoAlt),
                              onPressed: () {
                                _swipeKey.currentState.rewind();
                                print(_swipeKey.currentState
                                    .toDiagnosticsNode()
                                    .toStringDeep());
                              },
                            ),
                          ),
                        ),
                        FloatingActionButton(
                          heroTag: "${profile.surnames}22",
                          backgroundColor: Colors.white,
                          foregroundColor: Colors.purple.shade400,
                          child: FaIcon(FontAwesomeIcons.times),
                          onPressed: () async {
                            _swipeKey.currentState.swipeLeft();
                          },
                        ),
                        FloatingActionButton(
                          heroTag: "${profile.surnames}33",
                          backgroundColor: Colors.white,
                          foregroundColor: Colors.redAccent,
                          child: Icon(
                            EvaIcons.heart,
                          ),
                          onPressed: () async {
                            _swipeKey.currentState.swipeRight();
                          },
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
          Divider(
            color: Colors.transparent,
          ),
        ],
      ),
    );
  }
}



class Profile {
  int idUser;
  String names;
  String surnames;
  String age;
  String country;
  String city;
  String location;
  String description;
  String interests;
  String lifeStyle;
  String lookingFor;
  String interestedIn;
  String gender;
  String job;
  // List<dynamic> userImages;
  List<dynamic> userImages;

  Profile(
      {this.idUser,
      this.names,
      this.surnames,
      this.age,
      this.country,
      this.city,
      this.location,
      this.description,
      this.interests,
      this.lifeStyle,
      this.lookingFor,
      this.interestedIn,
      this.gender,
      this.job,
      this.userImages});

  int get getIdUser => idUser;
  String get getNames => names;
  String get getSurnames => surnames;
  String get getAge => age;
  String get getCountry => country;
  String get getCity => city;
  String get getLocation => location;
  String get getDescription => description;
  String get getInterests => interests;
  String get getLifeStyle => lifeStyle;
  String get getLookingFor => lookingFor;
  String get getInterestedIn => interestedIn;
  String get getGender => gender;
  String get getJob => job;
  List<dynamic> get getimageUsers => userImages;
}
