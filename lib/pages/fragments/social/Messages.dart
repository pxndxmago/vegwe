import 'dart:math';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vegwe/pages/fragments/social/Profile.dart';
import 'package:vegwe/providers/AdMobService.dart';
import 'package:vegwe/providers/api_provider.dart';

class MessagesFragment extends StatefulWidget {
  MessagesFragment({Key key}) : super(key: key);

  @override
  _MessagesFragmentState createState() => _MessagesFragmentState();
}

class _MessagesFragmentState extends State<MessagesFragment>
    with AutomaticKeepAliveClientMixin {
  // List chats = new List();
  Map user;
  List<dynamic> chats = [];
  SocialPageBloc bloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = Provider.of(context);
    bloc.chatsStream.listen((data) {
      chats = data;
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
      onRefresh: _getChats,
      child: FutureBuilder(
        future: _builChatList(),
        builder: (context, snapshot) {
          if (snapshot.data != null)
            return snapshot.data;
          else
            return _buildNoChatsPoster();
        },
      ),
    );
  }

  Future<void> _getChats() async {
    print("ejecutando _getChats");
    Map responseObject = await ApiProvider.api.getChats();
    if (responseObject['status'] == "success") {
      bloc.changeChats(responseObject['chats']);
      if (mounted) setState(() {});
    }
  }

  Future<Widget> _builChatList() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    int loggedUserId = pref.getInt('idUser');
    var itemCounter = 0;
    return chats.isNotEmpty
        ? ListView.builder(
            // physics: ScrollPhysics(),
            itemCount: chats.length,
            itemBuilder: (BuildContext context, int index) {
              loopBegin:
              if (itemCounter == 5) {
                itemCounter = 0;
                return AdmobBanner(
                  adUnitId: AdMobService.admob.getBannerId(),
                  adSize: AdmobBannerSize.BANNER,
                  listener: (AdmobAdEvent event, Map<String, dynamic> args) {
                    switch (event) {
                      case AdmobAdEvent.loaded:
                        break;

                      case AdmobAdEvent.opened:
                        print('Admob banner opened!');
                        break;

                      case AdmobAdEvent.closed:
                        print('Admob banner closed!');
                        break;

                      case AdmobAdEvent.failedToLoad:
                        print(
                            'Admob banner failed to load. Error code: ${args['errorCode']}');
                        break;
                      case AdmobAdEvent.clicked:
                        // TODO: Handle this case.
                        break;
                    }
                  },
                );
                break loopBegin;
              }
              var actualChat = chats[index];
              var _chatName = "";
              var _idUserChat = 0;
              var chatImage = CircleAvatar(
                backgroundColor: Colors.blue,
              );
              var lastMessage = "";
              if (actualChat['users'].length == 2) {
                for (var user in actualChat['users']) {
                  if (user['idUser'] != loggedUserId) {
                    _chatName = user['names'];
                    _idUserChat = user['idUser'];
                    chatImage = CircleAvatar(
                      backgroundImage: ExtendedNetworkImageProvider(
                        "${ApiProvider.userImagesUrl}/${user['images'][0]['idImage']}",
                        cache: true,
                      ),
                    );
                    break;
                  }
                }
              } else {
                _idUserChat = 0;
                _chatName = actualChat['name'];
                chatImage = CircleAvatar(
                  child: Icon(MdiIcons.alphaG),
                );
              }
              lastMessage = actualChat['lastMessage'] != null
                  ? actualChat['lastMessage']['message']
                  : '';
              itemCounter++;
              return Card(
                elevation: 0,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                color: Colors.white70,
                child: ListTile(
                  leading: GestureDetector(
                    child: chatImage,
                    onTap: () {
                      if (_idUserChat > 0) {
                        Get.toNamed('profile',
                            arguments: ProfileArguments(_idUserChat));
                      }
                    },
                  ),
                  trailing: FaIcon(
                    FontAwesomeIcons.angleRight,
                    size: 18,
                  ),
                  title: Text(
                    _chatName,
                    style: Theme.of(context).textTheme.subtitle1.copyWith(),
                  ),
                  subtitle: Visibility(
                      visible: lastMessage != '',
                      child: Text(
                        lastMessage,
                        overflow: TextOverflow.ellipsis,
                      )),
                  onTap: () async {
                    setState(() {
                      bloc.changeSelectedChat(actualChat['id']);
                      Get.toNamed('chat');
                    });
                  },
                ),
              );
            },
          )
        : _buildNoChatsPoster();
  }

  _buildNoChatsPoster() {
    return ListView(
      children: [
        Transform.rotate(
          angle: pi / 50,
          child: Container(
            // height: MediaQuery.of(context).size.height * 0.4,
            // width: MediaQuery.of(context).size.width * 0.2,
            margin: EdgeInsets.symmetric(horizontal: 50, vertical: 100),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              border: Border.all(width: 10, color: Colors.teal.shade400),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.2,
                  child: Image.asset("assets/zanahoria_triste.png"),
                ),
                Text(
                  'No tienes conversaciones con nadie aún',
                  textAlign: TextAlign.center,
                ),
                FlatButton(
                  child: Text(
                    'Empieza a conocer gente',
                  ),
                  color: Colors.white,
                  onPressed: () {},
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
