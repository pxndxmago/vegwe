import 'dart:async';
import 'dart:convert';
import 'package:bubble/bubble.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:vegwe/pages/blocs/provider.dart';
// import 'package:vegwe/pages/fragments/custom_emoji_picker.dart';
import 'package:vegwe/providers/api_provider.dart';
import 'package:vegwe/providers/db_provider.dart';
import 'package:vegwe/providers/socketIO_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:list_view_item_builder/list_view_item_builder.dart';
import 'package:vegwe/pages/fragments/social/Profile.dart';

class ChatArguments {
  final int idUser;

  ChatArguments(this.idUser);
}

class ChatPage extends StatefulWidget {
  // final SocialPageBloc bloc;
  // final int idUser;
  ChatPage({
    Key key,
    // this.idUser,
    // this.bloc,
  }) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  // SocketIO socketIO;
  TextEditingController inputMsg;
  bool _isShowKeyboard = false;
  bool _isShowEmojiKeyboard = false;
  int conn = 0;
  int lastIdMessage = 0;
  List<Message> listMessages = [];
  SocialPageBloc bloc;
  List<Widget> lista = [];
  ScrollController _scrollController = new ScrollController();
  ListViewItemBuilder _itemBuilder;
  Map _actualChat;
  String chatName = "";
  int idUserChat = 0;

  // String chatImageLink ;
  var chatImage = CircleAvatar(
    backgroundColor: Colors.blue,
  );

  @override
  void initState() {
    super.initState();
    inputMsg = new TextEditingController();

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        _isShowKeyboard = visible;
        if (_isShowKeyboard) _isShowEmojiKeyboard = false;
        // _goToLastMessage();
        setState(() {});
      },
    );
    // Timer(Duration(seconds: 1), _goToLastMessage);

    // bloc.selectedChatStream.listen((selectedChat) async {});

    _scrollController.addListener(() {
      print(_scrollController.position);
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc ??= Provider.of(context);
    bloc.messagesStream.listen((messages) {
      print("Se recibe un mensaje");
      listMessages = messages != null ? messages : [];
      if (mounted) setState(() {});
    });
    loadData();
    _getMessagesFromApi();
  }
  
  loadData() async {
    Map response = await ApiProvider.api.getChat(bloc.selectedChat);
    if (response['status'] == 'success') {
      SharedPreferences pref = await SharedPreferences.getInstance();
      int loggedUserId = pref.getInt('idUser');
      _actualChat = response['chat'];

      if (_actualChat != null && _actualChat.length > 0) {
        if (_actualChat['users'].length == 2) {
          for (var user in _actualChat['users']) {
            if (user['idUser'] != loggedUserId) {
              idUserChat = user['idUser'];
              chatName = user['names'];
              chatImage = CircleAvatar(
                backgroundImage: ExtendedNetworkImageProvider(
                  "${ApiProvider.userImagesUrl}/${user['images'][0]['idImage']}",
                  cache: true,
                  // fit: BoxFit.cover,
                ),
              );
              break;
            }
          }
        } else {
          idUserChat = 0;
          chatName = _actualChat['name'];
          chatImage = CircleAvatar(
            child: Icon(MdiIcons.alphaG),
          );
        }
      }
      if (mounted) setState(() {});
    }
  }

  @override
  void dispose() {
    print("dispose de chat ejecutado");
    bloc.changeMessages([]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // bloc.chatsStream.listen((data) {});

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        titleSpacing: 0.0,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: FaIcon(
            FontAwesomeIcons.chevronLeft,
            color: Colors.black54,
          ),
        ),
        title: FutureBuilder(
          future: _getChatHeader(),
          builder: (context, snapshot) {
            return snapshot.data != null ? snapshot.data : Container();
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  colorFilter:
                      ColorFilter.mode(Colors.white, BlendMode.colorBurn),
                  image: AssetImage('assets/valley3xxhdpi.png'),
                  fit: BoxFit.fill),
            ),
          ),
          SafeArea(
            top: true,
            child: Container(
              child: Column(
                children: <Widget>[
                  Expanded(
                    // height: MediaQuery.of(context).size.height * 0.95,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        constraints: BoxConstraints.expand(
                            height: MediaQuery.of(context).size.height * 0.57),
                        // child: _getListView(),
                        child: Builder(
                          builder: (BuildContext context) {
                            return RefreshIndicator(
                              onRefresh: _getMessagesFromApi,
                              child: ListView.custom(
                                padding: EdgeInsets.symmetric(vertical: 8),
                                reverse: false,
                                physics: AlwaysScrollableScrollPhysics(),
                                controller: _scrollController,
                                childrenDelegate: SliverChildBuilderDelegate(
                                    (context, index) {
                                      return MessageItem(
                                          key: ValueKey(listMessages[index]),
                                          message: listMessages[index]);
                                    },
                                    childCount: listMessages.length,
                                    findChildIndexCallback: (Key key) {
                                      final ValueKey valueKey = key;
                                      final Message data = valueKey.value;
                                      return listMessages.indexOf(data);
                                    }),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                  Card(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    margin: EdgeInsets.only(left: 15, right: 15, bottom: 5),
                    child: _getTextFieldChatItem(),
                  ),
                  // (_isShowEmojiKeyboard)?_buildEmojiPicker():Container(),
                  // Visibility(
                  //   visible: _isShowEmojiKeyboard,
                  //   child: _buildEmojiPicker(),
                  //   // maintainSemantics: true,
                  //   // maintainSize: true,
                  //   // maintainAnimation: true,
                  //   // maintainInteractivity: true,
                  //   maintainState: true,
                  // ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Widget _buildEmojiPicker() {
  //   try {
  //     return Container(
  //       // padding: EdgeInsets.only(top: 10),
  //       // height: 150,
  //       width: double.infinity,
  //       child: CustomEmojiPicker(
  //         rows: 3,
  //         columns: 7,
  //         buttonMode: ButtonMode.CUPERTINO,
  //         // recommendKeywords: ["racing", "horse"],
  //         numRecommended: 10,
  //         onEmojiSelected: (emoji, category) {
  //           int currentPosition =
  //               inputMsg.selection.start >= 0 ? inputMsg.selection.start : 0;

  //           String appendedText = '';
  //           String firstPart = '';
  //           firstPart =
  //               inputMsg.text.substring(0, currentPosition) + emoji.emoji;
  //           appendedText = firstPart + inputMsg.text.substring(currentPosition);
  //           // if (currentPosition != -1) {
  //           // } else {
  //           //   appendedText = "${inputMsg.text}${emoji.emoji}";
  //           // }

  //           inputMsg.value = inputMsg.value.copyWith(
  //             text: appendedText,
  //             selection: TextSelection.collapsed(
  //                 offset: currentPosition == appendedText.length
  //                     ? appendedText.length
  //                     : firstPart.length),
  //           );
  //         },
  //       ),
  //     );
  //   } catch (e) {}
  // }

  Widget _getTextFieldChatItem() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          IconButton(
            icon: FaIcon(FontAwesomeIcons.smileWink),
            onPressed: () {
              SystemChannels.textInput.invokeMethod('TextInput.hide');
              _isShowEmojiKeyboard = !_isShowEmojiKeyboard;
              setState(() {});
            },
          ),
          Expanded(
            flex: 3,
            child: TextField(
              controller: inputMsg,
              decoration: new InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  contentPadding:
                      EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                  hintText: 'Escribe algo'),
              keyboardType: TextInputType.multiline,
              maxLines: 3,
              minLines: 1,
              textInputAction: TextInputAction.send,
              onSubmitted: (String mensaje) {
                _sendMessage();
              },
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.send,
            ),
            onPressed: () {
              _sendMessage();
            },
          ),
        ],
      ),
    );
  }

  void _sendMessage() async {
    Map message = new Map();
    message['message'] = inputMsg.text.trim();
    message['idChat'] = bloc.selectedChat;
    if (inputMsg.text.trim() != "") {
      String jsonData = json.encode(message);
      (await SocketIOProvider.io.socketIO())
          .sendMessage("send message", jsonData);
      inputMsg.clear();
    }
  }

  Future<Null> _getMessagesFromApi() async {
    // print("Se ejecuta _getMessagesFromApi");

    dynamic _idMessage =
        listMessages.length > 0 ? listMessages[0].idMessage : "";
    try {
      Map responseObject =
          await ApiProvider.api.getMessages(bloc.selectedChat, _idMessage);

      if (responseObject['status'] == 'success') {
        var messages = responseObject['messages'];
        for (var message in messages) {
          final newMessage = Message(
            idChat: message['idChat'],
            idMessage: message['idMessage'],
            idSender: message['idSender'],
            message: message['message'],
            createdAt: message['createdAt'],
          );
          await DBProvider.db.insertMessage(newMessage);
        }
      }
    } catch (e) {
      print("Error on _getMessagesFromApi method $e");
    }

    _getMessagesFromDB();
  }

  void _getMessagesFromDB() async {
    // print("Se ejecuta _getMessagesFromDB");

    int _idLastMessage =
        listMessages.length > 0 ? listMessages[0].idMessage : 0;
    List<Message> messages = await DBProvider.db
        .getMessagesByChat(bloc.selectedChat, _idLastMessage, 10);
    if (messages.isNotEmpty) {
      for (var message in messages) {
        List<Message> blocMessages = bloc.messages;

        if (blocMessages.isEmpty ||
            blocMessages.first.idMessage > message.idMessage) {
          blocMessages.insert(0, message);
        }

        bloc.changeMessages(blocMessages);
      }
    }
  }

  Future<Widget> _messageContainer(Message message) async {
    return MessageItem(key: ValueKey(message), message: message);
  }

  Future<Widget> _getChatHeader() async {
    return ListTile(
      contentPadding: EdgeInsets.all(0.0),
      leading: GestureDetector(
        child: chatImage,
        onTap: () {
          if (idUserChat > 0) {
            Get.toNamed('profile', arguments: ProfileArguments(idUserChat));
          }
        },
      ),
      title: Text(
        chatName,
        style: Theme.of(context)
            .textTheme
            .headline6
            .copyWith(color: Colors.black54),
      ),
    );
  }
}

class MessageItem extends StatefulWidget {
  final Message message;
  const MessageItem({Key key, this.message}) : super(key: key);

  @override
  _MessageItemState createState() => _MessageItemState();
}

class _MessageItemState extends State<MessageItem>
    with AutomaticKeepAliveClientMixin {
  int loggedUserId;
  @override
  void initState() {
    super.initState();
    print("Rebuild for ${((widget.key as ValueKey).value as Message).message}");
    loadData();
  }

  loadData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    loggedUserId = pref.getInt('idUser');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Color colorReceptor = Color.fromARGB(255, 86, 121, 242);
    Color colorRemitente = Colors.white;
    double _messageInternalPadding = 10;
    double _messageBorderRadius = 10;
    double _marginTopBubblue = 8.0;

    return loggedUserId != null
        ? Bubble(
            radius: Radius.circular(_messageBorderRadius),
            padding: BubbleEdges.all(_messageInternalPadding),
            alignment: widget.message.idSender != loggedUserId
                ? Alignment.topLeft
                : Alignment.topRight,
            color: widget.message.idSender != loggedUserId
                ? colorRemitente
                : colorReceptor,
            margin: BubbleEdges.only(top: _marginTopBubblue, right: 25),
            child: Text(
              widget.message.message,
              style: TextStyle(
                  fontSize: 16,
                  color: widget.message.idSender != loggedUserId
                      ? Colors.black54
                      : Colors.white),
            ),
            nip: BubbleNip.no,
          )
        : Container();
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
