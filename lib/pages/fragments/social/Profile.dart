import 'package:easy_localization/easy_localization.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_tr/flutter_tr.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:smart_select/smart_select.dart';
import 'package:vegwe/SharedFunctions.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/providers/api_provider.dart';
import 'package:vegwe/pages/fragments/social/Gallery.dart';
import 'package:get/get.dart';

class ProfileArguments {
  final int idUser;

  ProfileArguments(this.idUser);
}

class ProfilePage extends StatefulWidget {
  final int idUser;
  ProfilePage({Key key, this.idUser}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  double _containerIconInteresesSize = 40.0;
  String _names = "";
  String _surnames = "";
  String _occupation = "";
  String _country = "";
  String _city = "";
  String _gender = "";
  String _lifeStyle = "";
  String _description = "";
  int _age = 0;
  List<String> _interests = [];
  List<String> _languages = [];
  Map<String, String> interestsOptions = {};
  List<SmartSelectOption<String>> languagesOptions;
  List<dynamic> userImages;
  dynamic _profileImage = Image.asset('assets/images/transparent.png');
  SocialPageBloc bloc;
  bool interestsLoading;
  String _distance = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userImages = [];
    loadData();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    bloc ??= Provider.of(context);
    await _getInterests();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: createChat,
        child: Icon(MdiIcons.message),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.white,
            expandedHeight: MediaQuery.of(context).size.height * 0.5,
            flexibleSpace: FlexibleSpaceBar(
              background: Stack(children: [
                Container(
                  height: Get.height,
                  width: Get.width,
                  child: _profileImage,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FloatingActionButton(
                          heroTag: "$_names-22",
                          backgroundColor: Colors.white,
                          foregroundColor: Colors.purple.shade400,
                          child: FaIcon(FontAwesomeIcons.times),
                          onPressed: () async {
                            Map response = await ApiProvider.api
                                .sendLike(widget.idUser, 0);
                            if (response['status'] == 'success') {
                              SharedFunctions.f.showSnackbar('Vegwe', 'Dislike enviado');
                            }
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FloatingActionButton(
                          heroTag: "$_names-33",
                          backgroundColor: Colors.white,
                          foregroundColor: Colors.redAccent,
                          child: Icon(
                            EvaIcons.heart,
                          ),
                          onPressed: () async {
                            Map response = await ApiProvider.api
                                .sendLike(widget.idUser, 1);
                            if (response['status'] == 'success') {
                              SharedFunctions.f.showSnackbar('Vegwe', 'Like enviado');
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          _gender == 'male'
                              ? MdiIcons.genderMale
                              : MdiIcons.genderFemale,
                          color: _gender == 'male' ? Colors.cyan : Colors.pink,
                          size: 20,
                        ),
                        Text(
                          "$_names $_surnames${_age > 0 ? ', $_age' : ''}",
                          style: Theme.of(context).textTheme.headline5.copyWith(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    tr('titles.$_lifeStyle'),
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.all(12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        EvaIcons.pin,
                        size: 15,
                        color: Colors.red.shade400,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          'Guayaquil - Ecuador',
                          style: Theme.of(context).textTheme.bodyText1.copyWith(
                                color: Colors.black87,
                                // fontWeight: FontWeight.bold,
                              ),
                        ),
                      ),
                    ],
                  ),
                  Text(_distance)
                ],
              ),
            ),
          ),
          _occupation != ''
              ? SliverToBoxAdapter(
                  child: Container(
                    padding: EdgeInsets.all(12),
                    child: Row(
                      children: <Widget>[
                        FaIcon(
                          FontAwesomeIcons.briefcase,
                          size: 15,
                          color: Colors.blueGrey.shade400,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text(
                            _occupation,
                            style:
                                Theme.of(context).textTheme.bodyText1.copyWith(
                                      color: Colors.black87,
                                      // fontWeight: FontWeight.bold,
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : SliverToBoxAdapter(),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Divider(
                color: Colors.grey,
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    tr('titles.description'),
                    style: Theme.of(context).textTheme.headline6.copyWith(
                          color: Colors.orange,
                          // fontWeight: FontWeight.bold,
                        ),
                  ),
                  Divider(color: Colors.transparent),
                  Text(
                    _description != ''
                        ? _description
                        : tr('messages.noDescription'),
                    style: Theme.of(context).textTheme.bodyText1.copyWith(
                          color: Colors.black87,
                          // fontWeight: FontWeight.bold,
                        ),
                  ),
                ],
              ),
              // Gallery()
            ),
          ),
          SliverToBoxAdapter(
            child: Visibility(
              visible: _interests.length > 0,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      tr('titles.interests'),
                      style: Theme.of(context).textTheme.headline6.copyWith(
                            color: Colors.orange,
                            // fontWeight: FontWeight.bold,
                          ),
                    ),
                    Divider(color: Colors.transparent),
                    Wrap(
                      spacing: 5,
                      children: _interests.map((interest) {
                        return Chip(
                          backgroundColor: Colors.indigo.shade500,
                          elevation: 3,
                          label: Text(
                            interest,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(color: Colors.white),
                          ),
                        );
                      }).toList(),
                    ),
                  ],
                ),
                // Gallery()
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Visibility(
              visible: _languages.length > 0,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      tr('titles.languages'),
                      style: Theme.of(context).textTheme.headline6.copyWith(
                            color: Colors.deepPurple,
                            // fontWeight: FontWeight.bold,
                          ),
                    ),
                    Divider(color: Colors.transparent),
                    Wrap(
                      spacing: 5,
                      children: _languages.map((language) {
                        return Chip(
                          backgroundColor: Colors.indigo.shade500,
                          elevation: 3,
                          label: Text(
                            language,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(color: Colors.white),
                          ),
                        );
                      }).toList(),
                    ),
                  ],
                ),
                // Gallery()
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                tr('titles.photos'),
                style: Theme.of(context).textTheme.headline6.copyWith(
                      color: Colors.deepOrange,
                      // fontWeight: FontWeight.bold,
                    ),
              ),
              // Gallery()
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.all(2.0),
            sliver: SliverGrid(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 160.0,
                mainAxisSpacing: 2.0,
                crossAxisSpacing: 2.0,
                childAspectRatio: 1.0,
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return GestureDetector(
                    child: Container(
                      color: Colors.grey.shade900,
                      child: ExtendedImage.network(
                        "${ApiProvider.userImagesUrl}/${userImages[index]['idImage']}",
                        cache: true,
                        fit: BoxFit.cover,
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, 'gallery',
                          arguments: GalleryArguments(userImages, index));
                    },
                  );
                },
                childCount: userImages.length,
              ),
            ),
          ),
        ],
      ),
    );
  }

  loadUserImages(Map responseObject) async {
    userImages.clear();
    userImages = responseObject['user']['image_users'];
    if (userImages.isNotEmpty) {
      _profileImage = ExtendedImage.network(
        "${ApiProvider.userImagesUrl}/${userImages[0]['idImage']}",
        cache: true,
        fit: BoxFit.cover,
      );
    }
  }

  createChat() async {
    Map response = await ApiProvider.api.createChat(widget.idUser);
    if (response['status'] == 'success') {
      _getChats();
      bloc.changeSelectedChat(response['chat']['id']);
      Navigator.pushNamed(context, 'chat', arguments: bloc);
    }
  }

  void _getChats() async {
    Map responseObject = await ApiProvider.api.getChats();
    if (responseObject['status'] == "success") {
      bloc.changeChats(responseObject['chats']);
      setState(() {});
    }
  }

  loadData() async {
    print("Fetching data");
    try {
      Map responseObject = await ApiProvider.api.getUser(this.widget.idUser);
      if (responseObject['status'] == 'success') {
        loadUserImages(responseObject);
        _names = responseObject['user']['names'] ?? "";
        _surnames = responseObject['user']['surnames'] ?? "";
        _occupation = responseObject['user']['job'] ?? "";
        _gender = responseObject['user']['gender'] ?? "";
        _lifeStyle = responseObject['user']['lifeStyle'] ?? "";
        _description = responseObject['user']['description'] ?? "";
        _occupation = responseObject['user']['job'] ?? "";
        await _getInterests();
        if (responseObject['user']['interests'] != null)
          for (var interest
              in responseObject['user']['interests'].toString().split(',')) {
            if (interest != '') _interests.add(interestsOptions[interest]);
          }

        if (responseObject['user']['languages'] != null)
          for (var language
              in responseObject['user']['languages'].toString().split(',')) {
            if (language != '') _languages.add(tr('languages.$language'));
          }

        _age = SharedFunctions.f.calculateAge(responseObject['user']['bornDate']);
        if (responseObject['user']['distance'] != null) {
          var dist = responseObject['user']['distance'];
          if (dist > 1000) {
            print(dist);
            _distance = '${(dist / 1000).truncate()} Km';
          } else {
            _distance = '${(dist / 1000).truncate()} m';
          }
          print(_distance);
        }

        if (mounted) setState(() {});
      } else {
        Navigator.of(context).pop();
        // _showNotification(context, "Error al cargar los datos");
      }
    } catch (e) {
      print('Error en metodo loadData: ');
      print(e);
    }
  }

  _getInterests() async {
    Map resp = await ApiProvider.api.getInterests(context);
    if (resp['status'] == 'success') {
      for (var interest in resp['interests']) {
        interestsOptions[interest['cod']] = interest['text'];
      }
    }
  }
}
