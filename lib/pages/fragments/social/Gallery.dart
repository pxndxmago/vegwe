import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:extended_image/extended_image.dart';
import 'package:vegwe/providers/api_provider.dart';

class GalleryArguments {
  final List<dynamic> userImages;
  final int currentIndex;
  GalleryArguments(this.userImages, this.currentIndex);
}

class Gallery extends StatelessWidget {
  final List<dynamic> userImages;
  int currentIndex;
  Gallery({Key key, this.userImages, this.currentIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: ExtendedImageGesturePageView.builder(
        itemBuilder: (BuildContext context, int index) {
          var url =
              "${ApiProvider.userImagesUrl}/${userImages[index]['idImage']}";
          Widget image = ExtendedImage.network(
            url,
            fit: BoxFit.contain,
            mode: ExtendedImageMode.gesture,
            initGestureConfigHandler: (imageState) {
              // print(imageState);
              return GestureConfig(
                  // speed: 5,
                  inPageView: true,
                  initialScale: 1.0,
                  //you can cache gesture state even though page view page change.
                  //remember call clearGestureDetailsCache() method at the right time.(for example,this page dispose)

                  cacheGesture: false);
            },
          );
          image = Container(
            color: Colors.black87,
            child: image,
            // padding: EdgeInsets.all(5.0),
          );
          if (index == currentIndex) {
            return Hero(
              tag: url + index.toString(),
              child: image,
            );
          } else {
            return image;
          }
        },
        itemCount: userImages.length,
        onPageChanged: (int index) {
          currentIndex = index;
          // rebuild.add(index);
        },
        controller: PageController(
          initialPage: currentIndex,
        ),
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
