import 'package:admob_flutter/admob_flutter.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:vegwe/SharedFunctions.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/pages/fragments/social/Profile.dart';
import 'package:vegwe/providers/AdMobService.dart';
import 'package:vegwe/providers/api_provider.dart';

class MatchesFragment extends StatefulWidget {
  MatchesFragment({Key key}) : super(key: key);

  @override
  _MatchesFragmentState createState() => _MatchesFragmentState();
}

class _MatchesFragmentState extends State<MatchesFragment>
    with AutomaticKeepAliveClientMixin {
  // List chats = new List();
  Map user;
  List<dynamic> likes = [];
  int expandedItem;
  SocialPageBloc bloc;

  @override
  void initState() {
    super.initState();
    loadLikes();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = Provider.of(context);
  }

  loadLikes() async {
    Map response = await ApiProvider.api.getLikes();
    if (response['status'] == 'success') {
      likes = response['likes'];
    }
    if (mounted) setState(() {});
    List templikes = [];
    for (var i = 0; i < 10; i++) {
      for (var like in likes) {
        templikes.add(like);
      }
    }
    if (mounted)
      setState(() {
        likes = templikes;
      });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ExpandableList();
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class ExpandableList extends StatefulWidget {
  @override
  _ExpandableListState createState() => _ExpandableListState();
}

class _ExpandableListState extends State<ExpandableList> {
  List likesList = [];

  List viewsList = [];

  @override
  void initState() {
    super.initState();
    loadMatches();
  }

  Future<Null> loadMatches() async {
    Map response = await ApiProvider.api.getLikes();
    if (response['status'] == 'success') {
      likesList = response['likes'];
      if (mounted) setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    int itemCounter = 0;
    return RefreshIndicator(
      onRefresh: loadMatches,
      child: ListView(children: [
        ExpansionTile(
          title: Text("Likes"),
          children: likesList.map((val) {
            loopBegin:
            if (itemCounter == 10) {
              itemCounter = 0;
              return AdmobBanner(
                adUnitId: AdMobService.admob.getBannerId(),
                adSize: AdmobBannerSize.BANNER,
                listener: (AdmobAdEvent event, Map<String, dynamic> args) {
                  switch (event) {
                    case AdmobAdEvent.loaded:
                      break;

                    case AdmobAdEvent.opened:
                      print('Admob banner opened!');
                      break;

                    case AdmobAdEvent.closed:
                      print('Admob banner closed!');
                      break;

                    case AdmobAdEvent.failedToLoad:
                      print(
                          'Admob banner failed to load. Error code: ${args['errorCode']}');
                      break;
                    case AdmobAdEvent.clicked:
                      // TODO: Handle this case.
                      break;
                  }
                },
              );
              break loopBegin;
            }
            itemCounter++;

            return new ListTile(
              onTap: () {
                Navigator.pushNamed(context, 'profile',
                    arguments: ProfileArguments(val['idUserLiked']));
              },
              leading: CircleAvatar(
                backgroundImage: ExtendedNetworkImageProvider(
                  "${ApiProvider.userImagesUrl}/${val['user']['image_users'][0]['idImage']}",
                  cache: true,

                  // fit: BoxFit.cover,
                ),
              ),
              title: new Text(
                  "${val['user']['names']}, ${SharedFunctions.f.calculateAge(val['user']['bornDate'])}"),
            );
          }).toList(),
        ),
        ExpansionTile(
          title: Text("Visitas"),
          children: viewsList
              .map((val) => new ListTile(
                    title: new Text(val),
                  ))
              .toList(),
        ),
      ]),
    );
  }
}
