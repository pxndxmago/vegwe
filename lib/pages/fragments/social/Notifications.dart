import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:vegwe/pages/blocs/provider.dart';
import 'package:vegwe/pages/fragments/social/Matches.dart';
import 'package:vegwe/pages/fragments/social/Messages.dart';

class NotificationsPage extends StatefulWidget {
  NotificationsPage({Key key}) : super(key: key);

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage>
    with SingleTickerProviderStateMixin {
  SocialPageBloc bloc;
  TabController tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(initialIndex: 0, length: 2, vsync: this);
    tabController.addListener(() {
      print(tabController.index);
      if (mounted) setState(() {});
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = Provider.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        titleSpacing: 0,
        title: TabBar(
          controller: tabController,
          indicatorSize: TabBarIndicatorSize.label,
          isScrollable: true,
          tabs: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              child: Tab(
                child: Icon(
                  tabController.index == 0
                      ? MdiIcons.forumOutline
                      : MdiIcons.forum,
                  color: Colors.cyan,
                  size: 30,
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              child: Tab(
                child: Icon(
                  MdiIcons.fire,
                  color: Colors.redAccent,
                  size: 30,
                ),
              ),
            )
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        // physics: NeverScrollableScrollPhysics(),
        children: <Widget>[MessagesFragment(), MatchesFragment()],
      ),
    );
  }
}
