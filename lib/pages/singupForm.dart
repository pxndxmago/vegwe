import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:intl/intl.dart';
import 'package:vegwe/providers/api_provider.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpForm extends StatefulWidget {
  final String email;
  SignUpForm({Key key, this.email}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState(email);
}

class _SignUpFormState extends State<SignUpForm> {
//imagen
  Future<File> file;
  String statusImage = '';
  String base64Image = '';
  File tmpFile;
//
  String email;

  _SignUpFormState(String emailInfo) {
    this.email = emailInfo;
  }

  /////BOTONES DE CONTINUAR
  final GlobalKey<FormState> _passwordUser = new GlobalKey();
  bool _datosUsuariosDisabled;
  bool _ageUp;
  bool _generoPreferenciaDisabled;
  bool _queBusco;
  bool _dieta;

  ///

  ///Textfields
  ///datos Usuarios
  String _nombres;
  String _apellidos;

  ///fecha nacimiento
  String _dateTime;

  ///generoPreferencia
  String _gender;
  String _iLike;

  ///lookingFor
  String _lookingFor;

  ///dieta/estilo de vida
  String _dietaEstilo;

  ///password
  String _password;

  ///upload end point

  ///
  @override
  void initState() {
    print(this.email);
    super.initState();
    _datosUsuariosDisabled = true;
    _ageUp = true;
    _generoPreferenciaDisabled = true;
    _queBusco = true;
    _dieta = true;
    //textfields
    //datos Usuarios
    _nombres = "";
    _apellidos = "";
    //Fecha nacimiento
    //generoPreferencia?
    _gender = "";
    _iLike = "";
    //_lookingFor
    _lookingFor = "";
    //_dietaEstilo;
    _dietaEstilo = "";
    //password
    _password = "";
  }

  //funciones y metodos de validacion
  _datosUsuarios() {
    print(_nombres);
    print(_apellidos);
    if (_nombres.length != 0 && _apellidos.length != 0) {
      _datosUsuariosDisabled = false;
    }
    if (_nombres.length == 0 && _apellidos.length == 0) {
      _datosUsuariosDisabled = true;
    }
  }

  //
  calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    if (age < 18) {
      _ageUp = true;
    }
    if (age >= 18) {
      _ageUp = false;
    }
  }

  _generoPreferencia() {
    if (_gender.length > 0 && _iLike.length > 0) {
      _generoPreferenciaDisabled = false;
    }
    if (_gender.length == 0 && _iLike.length == 0) {
      _generoPreferenciaDisabled = true;
    }
  }

  _lookingForCheck() {
    if (_lookingFor.length > 0) {
      _queBusco = false;
    }
    if (_lookingFor.length == 0) {
      _queBusco = true;
    }
  }

  _dietaEstiloCheck() {
    if (_dietaEstilo.length > 0) {
      _dieta = false;
    }
    if (_dietaEstilo.length == 0) {
      _dieta = true;
    }
  }

  //
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  static int totalPages = 7;
  int totalDone = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Container(
          child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            StepProgressIndicator(
              totalSteps: totalPages,
              currentStep: totalDone + 1,
              size: 7,
              padding: 0,
              selectedColor: Colors.yellow,
              unselectedColor: Colors.transparent,
            ),
            Container(
              height: MediaQuery.of(context).size.height - 30,
              width: MediaQuery.of(context).size.width,
              child: Builder(
                builder: (BuildContext context) {
                  return PageView(
                    physics: NeverScrollableScrollPhysics(),
                    onPageChanged: (index) {},
                    controller: pageController,
                    children: <Widget>[
                      datosUsuario(context),
                      dateBorn(context),
                      generoPreferencia(context),
                      lookingForPage(context),
                      // picturePreference(context),
                      stadoComida(context),
                      savePassword(context)
                    ],
                  );
                },
              ),
            )
          ],
        ),
      )),
    ));
  }

  Widget datosUsuario(context) {
    return Container(
        color: Colors.transparent,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                  height: MediaQuery.of(context).size.height * 0.20,
                  child: Center(
                    child: Text(
                      "¿Como te llamas?",
                      style: TextStyle(
                          color: Colors.purple,
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold),
                    ),
                  )),
              Container(
                  height: MediaQuery.of(context).size.height * 0.604,
                  width: MediaQuery.of(context).size.width - 100,
                  child: Form(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextFormField(
                          initialValue: _nombres,
                          onChanged: (val) {
                            setState(() {
                              _nombres = val;
                            });
                            _datosUsuarios();
                          },
                          decoration: InputDecoration(
                            labelText: "Nombres",
                          ),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        TextFormField(
                          initialValue: _apellidos,
                          onChanged: (val) {
                            setState(() {
                              _apellidos = val;
                            });
                            _datosUsuarios();
                          },
                          decoration: InputDecoration(
                            labelText: "Apellidos",
                          ),
                        ),
                      ],
                    ),
                  )),
              _builBottomButtons(context, "Continuar", () {
                _datosUsuariosDisabled ? null : _nextPage();
              }, _datosUsuariosDisabled, true)
            ],
          ),
        ));
  }

  Widget dateBorn(context) {
    return Container(
        color: Colors.transparent,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.20,
                child: Center(
                  child: Text(
                    "¡Hola $_nombres!¿Cual es tu fecha de nacimiento?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.purple,
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.604,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(_dateTime == null
                          ? "Escoja su fecha de nacimiento "
                          : _dateTime.toString()),
                      IconButton(
                        color: Colors.grey.shade700,
                        icon: Icon(Icons.calendar_today),
                        onPressed: () {
                          DateTime date;
                          if (_dateTime != null && _dateTime != '') {
                            date = DateTime.parse(_dateTime);
                          } else {
                            date = DateTime.now();
                          }
                          CupertinoRoundedDatePicker.show(
                            context,
                            initialDate: date,
                            fontFamily: "Mali",
                            textColor: Colors.white,
                            background: Colors.red[300],
                            borderRadius: 16,
                            initialDatePickerMode: CupertinoDatePickerMode.date,
                            minimumYear: 1900,
                            onDateTimeChanged: (newDateTime) {
                              calculateAge(newDateTime);
                              var formatter = new DateFormat('yyyy-MM-dd');
                              String formatted = formatter.format(newDateTime);
                              setState(() {
                                _dateTime = formatted;
                              });
                            },
                          );
                        },
                      )
                    ],
                  ),
                ),
              ),
              _builBottomButtons(context, "Continuar", () {
                _ageUp
                    ? _showNotification(
                        context, 'Debe ser mayor de edad para crear una cuenta')
                    : _nextPage();
              }, _ageUp)
            ],
          ),
        ));
  }

  Widget generoPreferencia(context) {
    return Container(
        color: Colors.transparent,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.803,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(
                              "Genero",
                              style: TextStyle(
                                  color: Colors.purple,
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.bold),
                            ),
                            Wrap(
                              direction: Axis.horizontal,
                              children: <Widget>[
                                RaisedButton(
                                  padding: EdgeInsets.all(15.0),
                                  elevation: 7,
                                  color: _gender == "male"
                                      ? Colors.grey
                                      : Colors.white,
                                  onPressed: () {
                                    setState(() {
                                      _gender = "male";
                                    });
                                    _generoPreferencia();
                                  },
                                  child: Text("Masculino"),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        bottomLeft: Radius.circular(10)),
                                  ),
                                ),
                                RaisedButton(
                                  padding: EdgeInsets.all(15.0),
                                  elevation: 7,
                                  color: _gender == "female"
                                      ? Colors.grey
                                      : Colors.white,
                                  onPressed: () {
                                    setState(() {
                                      _gender = "female";
                                    });
                                    print(_gender);
                                    _generoPreferencia();
                                  },
                                  child: Text("Femenino"),
                                ),
                                RaisedButton(
                                  padding: EdgeInsets.all(15.0),
                                  elevation: 7,
                                  color: _gender == "other"
                                      ? Colors.grey
                                      : Colors.white,
                                  onPressed: () {
                                    setState(() {
                                      _gender = "other";
                                    });
                                    print(_gender);
                                    _generoPreferencia();
                                  },
                                  child: Text("Otro"),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(
                              "¿Busco?",
                              style: TextStyle(
                                  color: Colors.purple,
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.bold),
                            ),
                            Wrap(
                              direction: Axis.horizontal,
                              children: <Widget>[
                                RaisedButton(
                                  padding: EdgeInsets.all(15.0),
                                  elevation: 7,
                                  color: _iLike == "m"
                                      ? Colors.grey
                                      : Colors.white,
                                  onPressed: () {
                                    setState(() {
                                      _iLike = "m";
                                    });
                                    _generoPreferencia();
                                  },
                                  child: Text("Chicos"),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        bottomLeft: Radius.circular(10)),
                                  ),
                                ),
                                RaisedButton(
                                  padding: EdgeInsets.all(15.0),
                                  elevation: 7,
                                  color: _iLike == "f"
                                      ? Colors.grey
                                      : Colors.white,
                                  onPressed: () {
                                    setState(() {
                                      _iLike = "f";
                                    });
                                    _generoPreferencia();
                                  },
                                  child: Text("Chicas"),
                                ),
                                RaisedButton(
                                  padding: EdgeInsets.all(15.0),
                                  elevation: 7,
                                  color: _iLike == "b"
                                      ? Colors.grey
                                      : Colors.white,
                                  onPressed: () {
                                    setState(() {
                                      _iLike = "b";
                                    });
                                    _generoPreferencia();
                                  },
                                  child: Text("Ambos"),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10),
                                        bottomRight: Radius.circular(10)),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              _builBottomButtons(context, "Continuar", () {
                _generoPreferenciaDisabled ? null : _nextPage();
              }, _generoPreferenciaDisabled)
            ],
          ),
        ));
  }

  Widget lookingForPage(context) {
    return Container(
        child: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.803,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    "¿Que es lo que estas buscando?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.purple,
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Wrap(
                    direction: Axis.vertical,
                    children: <Widget>[
                      ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width - 90,
                        child: RaisedButton(
                          padding: EdgeInsets.all(15.0),
                          color:
                              _lookingFor == "n" ? Colors.grey : Colors.white,
                          onPressed: () {
                            setState(() {
                              _lookingFor = "n";
                            });
                            _lookingForCheck();
                          },
                          child: Text("Nada en concreto"),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width - 90,
                        child: RaisedButton(
                          padding: EdgeInsets.all(15.0),
                          color:
                              _lookingFor == "s" ? Colors.grey : Colors.white,
                          onPressed: () {
                            setState(() {
                              _lookingFor = "s";
                            });
                            _lookingForCheck();
                          },
                          child: Text("Sólo chatear"),
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width - 90,
                        child: RaisedButton(
                          padding: EdgeInsets.all(15.0),
                          color:
                              _lookingFor == "as" ? Colors.grey : Colors.white,
                          onPressed: () {
                            setState(() {
                              _lookingFor = "as";
                            });
                            _lookingForCheck();
                          },
                          child: Text("Algo serio"),
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width - 90,
                        child: RaisedButton(
                          padding: EdgeInsets.all(15.0),
                          color:
                              _lookingFor == "ac" ? Colors.grey : Colors.white,
                          onPressed: () {
                            setState(() {
                              _lookingFor = "ac";
                            });
                            _lookingForCheck();
                          },
                          child: Text("Algo casual"),
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          _builBottomButtons(
            context,
            "Continuar",
            () {
              _queBusco ? null : _nextPage();
            },
            _queBusco,
          ),
        ],
      ),
    ));
  }

  Widget picturePreference(context) {
    return Container(
      color: Colors.transparent,
      child: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.20,
            width: MediaQuery.of(context).size.width,
            child: Center(
              child: Text(
                "Muestranos una sonrisa\nSube una foto",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.purple,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.604,
            width: MediaQuery.of(context).size.width,
            child: Center(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.23,
                width: MediaQuery.of(context).size.width * 0.42,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    border: Border.all(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(10.0)),
                child: Stack(
                  children: <Widget>[
                    FutureBuilder<File>(
                      future: file,
                      builder:
                          (BuildContext context, AsyncSnapshot<File> snapshot) {
                        if (snapshot.connectionState == ConnectionState.done &&
                            null != snapshot.data) {
                          tmpFile = snapshot.data;
                          base64Image =
                              base64Encode(snapshot.data.readAsBytesSync());
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  border: Border.all(color: Colors.transparent),
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: MediaQuery.of(context).size.height,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: Colors.black,
                                        image: DecorationImage(
                                            image: FileImage(snapshot.data),
                                            fit: BoxFit.cover)),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomRight,
                                    child: Container(
                                      color: Colors.transparent,
                                      width: 40,
                                      child: ButtonTheme(
                                        minWidth: 10,
                                        child: RaisedButton(
                                            color: Color.fromRGBO(
                                                166, 28, 137, 0.65),
                                            padding: EdgeInsets.all(0.0),
                                            onPressed: () {
                                              setState(() {
                                                file = ImagePicker.pickImage(
                                                    source:
                                                        ImageSource.gallery);
                                              });
                                            },
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        100.0)),
                                            child: Icon(
                                              Icons.camera,
                                              color: Colors.white,
                                            )),
                                      ),
                                    ),
                                  )
                                ],
                              ));
                        } else if (null != snapshot.error) {
                          return Text(
                            "Error picking image",
                            textAlign: TextAlign.center,
                          );
                        } else {
                          return Container(
                              child: RaisedButton(
                            color: Color.fromRGBO(166, 28, 137, 0.40),
                            elevation: 0,
                            onPressed: () {
                              setState(() {
                                file = ImagePicker.pickImage(
                                    source: ImageSource.gallery);
                              });
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Stack(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.center,
                                  child: FaIcon(FontAwesomeIcons.cameraRetro,
                                      color: Colors.white),
                                ),
                                Align(
                                  alignment: Alignment(0.0, 0.50),
                                  child: Text(
                                    "Escoger una foto",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                )
                              ],
                            ),
                          ));
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          _builBottomButtons(
            context,
            "Continuar",
            () {
              _nextPage();
            },
            _queBusco,
          ),
        ],
      )),
    );
  }

  Widget stadoComida(context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.height * 0.20,
                child: Center(
                  child: Text(
                    "Dieta / Estilo de vida",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.purple,
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold),
                  ),
                )),
            Container(
              height: MediaQuery.of(context).size.height * 0.604,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width - 90,
                    child: RaisedButton(
                      padding: EdgeInsets.all(15.0),
                      color: _dietaEstilo == "interested" ? Colors.grey : Colors.white,
                      onPressed: () {
                        setState(() {
                          _dietaEstilo = "interested";
                        });
                        _dietaEstiloCheck();
                      },
                      child: Text("En transición"),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                  ),
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width - 90,
                    child: RaisedButton(
                      padding: EdgeInsets.all(15.0),
                      color:
                          _dietaEstilo == "vegan" ? Colors.grey : Colors.white,
                      onPressed: () {
                        setState(() {
                          _dietaEstilo = "vegan";
                        });
                        _dietaEstiloCheck();
                      },
                      child: Text("Vegano"),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                  ),
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width - 90,
                    child: RaisedButton(
                      padding: EdgeInsets.all(15.0),
                      color: _dietaEstilo == "vegetarian"
                          ? Colors.grey
                          : Colors.white,
                      onPressed: () {
                        setState(() {
                          _dietaEstilo = "vegetarian";
                        });
                        _dietaEstiloCheck();
                      },
                      child: Text("Vegetariano"),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            _builBottomButtons(
              context,
              "Continuar",
              () {
                _dieta ? null : _nextPage();
              },
              _dieta,
            ),
          ],
        ),
      ),
    );
  }

  Widget savePassword(context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.height * 0.20,
                child: Center(
                  child: Text(
                    "¡Ya es el ultimo paso!\n Danos una contraseña",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.purple,
                        fontSize: 25.0,
                        fontWeight: FontWeight.bold),
                  ),
                )),
            Container(
              height: MediaQuery.of(context).size.height * 0.604,
              width: MediaQuery.of(context).size.width - 100,
              child: Form(
                key: _passwordUser,
                autovalidate: true,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                      validator: (value) {
                        if (value.length < 8) {
                          return "La contraseña debe poseer mas de 8 caracteres";
                        } else {
                          return null;
                        }
                      },
                      obscureText: true,
                      decoration: InputDecoration(labelText: "Contraseña"),
                      onChanged: (val) {
                        setState(() {
                          _password = val;
                        });
                      },
                    ),
                    SizedBox(
                      height: 60.0,
                    ),
                    TextFormField(
                      obscureText: true,
                      decoration:
                          InputDecoration(labelText: "Repita la contraseña"),
                      validator: (value) {
                        if (value != _password) {
                          return "Las contraseñas no coinciden";
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            _builBottomButtons(
              context,
              "Continuar",
              () {
                if (_passwordUser.currentState.validate()) {
                  _signup(context);
                }
              },
              false,
            ),
          ],
        ),
      ),
    );
  }

  _nextPage() {
    pageController.nextPage(
        duration: Duration(milliseconds: 500), curve: Curves.bounceOut);
  }

  _showNotification(BuildContext context, String text) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
      ),
    );
  }

  _signup(BuildContext context) async {
    try {
      Map responseObject = await ApiProvider.api.signup(
          names: this._nombres,
          surnames: this._apellidos,
          bornDate: this._dateTime,
          gender: this._gender,
          interestedIn: this._iLike,
          lookingFor: this._lookingFor,
          lifeStyle: this._dietaEstilo,
          password: this._password,
          email: this.email);
      if (responseObject['status'] == 'success') {
        Map responseLogin =
            await ApiProvider.api.login(this.email, this._password);
        if (responseLogin['status'] == 'success')
          Navigator.of(context)
              .pushNamedAndRemoveUntil("social", ModalRoute.withName('login'));
        print('Cuenta creada');
      } else {
        print(responseObject['error']);

        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Text(responseObject['error']),
          ),
        );
      }
    } catch (e) {
      print("Error on _signup method: $e");
    }
  }

  Widget _builBottomButtons(
      BuildContext context, String text, Function callback, bool disabled,
      [bool isFirstPage = false]) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.10,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          isFirstPage
              ? Container()
              : Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      padding: EdgeInsets.all(15.0),
                      color: Colors.redAccent.shade100,
                      onPressed: () {
                        pageController.previousPage(
                            duration: Duration(milliseconds: 500),
                            curve: Curves.bounceOut);
                      },
                      elevation: 0,
                      child: FaIcon(FontAwesomeIcons.undoAlt),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  ),
                ),
          Expanded(
            flex: 4,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                padding: EdgeInsets.all(15.0),
                color: disabled ? Colors.grey : Colors.greenAccent,
                onPressed: callback,
                elevation: 0,
                child: Text(
                  text,
                  style: TextStyle(color: Colors.white, fontSize: 20.0),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
