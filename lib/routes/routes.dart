import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:vegwe/pages/Login.dart';
import 'package:vegwe/pages/fragments/social/Gallery.dart';
import 'package:vegwe/pages/fragments/social/Chat.dart';
import 'package:vegwe/pages/fragments/social/Profile.dart';
import 'package:vegwe/pages/introduction.dart';
import 'package:vegwe/pages/loginSignUp.dart';
import 'package:vegwe/pages/settings/cardsPreferences.dart';
import 'package:vegwe/pages/settings/settings.dart';
import 'package:vegwe/pages/settings/userSettings.dart';
import 'package:vegwe/pages/signUp.dart';
import 'package:vegwe/pages/singupForm.dart';
import 'package:vegwe/pages/social_page.dart';

getRoutesBuilderFunction(RouteSettings routeSettings) {
  PageTransitionType transition = PageTransitionType.slideInLeft;
  return PageRouteBuilder<dynamic>(
      settings: routeSettings,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        switch (routeSettings.name) {
          case 'login':
            return LoginPage();
            break;
          case 'social':
            return SocialPage();
            break;

          case 'chat':
            // ChatArguments args = routeSettings.arguments;

            return ChatPage();
            break;

          case 'profile':
            transition = PageTransitionType.transferUp;
            ProfileArguments args = routeSettings.arguments;
            return ProfilePage(
              idUser: args.idUser,
            );
            break;

          // case 'restaurantes':
          //   return RestaurantesPage();
          //   break;

          case 'gallery':
            GalleryArguments args = routeSettings.arguments;
            return Gallery(
              userImages: args.userImages,
              currentIndex: args.currentIndex,
            );
            break;

          case 'introduction':
            return IntroduccionPage();
            break;

          case 'loginSignUp':
            return LoginSignUpPage();
            break;

          case 'signUp':
            return SignUpPage();
            break;

          case 'signUpForm':
            return SignUpForm();
            break;

          case 'settings':
            return SettingsPage();
            break;

          case 'userSettings':
            return UserSettingsPage();
            break;
            
          case 'cardsPreferences':
            return CardsPreferencesPage();
            break;
        }
      },
      transitionDuration: Duration(milliseconds: 300),
      transitionsBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation, Widget child) {
        return effectMap[transition](
            Curves.linear, animation, secondaryAnimation, child);
      });
}
